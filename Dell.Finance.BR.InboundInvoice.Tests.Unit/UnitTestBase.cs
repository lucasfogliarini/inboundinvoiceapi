﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Dell.Finance.BR.InboundInvoice.Services;
using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Newtonsoft.Json;
using Xunit;
using System.Collections.Generic;
using Dell.Finance.BR.InboundInvoice.Infrastructure.Data;
using Dell.Finance.Framework.Core.Mailer.Data;
using Dell.Finance.Framework.Core.Mailer.Contracts;
using Dell.Finance.Framework.Core.Mailer.Service;
using System;
using MailKit.Net.Smtp;
using MailKit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Unit
{
    public class UnitTestBase
    {
        protected ServiceProvider ServiceProvider { get; set; }

        public UnitTestBase()
        {
            ConfigureServiceCollection();   
        }

        private void ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();
            AddAppSettings(serviceCollection);
            AddSmtpEmailconfiguration(serviceCollection);
            serviceCollection.AddFakeUnitOfWork();
            serviceCollection.AddServices();
            serviceCollection.AddValidator();
            serviceCollection.AddLogging();
            AddMailer(serviceCollection);
            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        protected void AddAppSettings(IServiceCollection services)
        {
            AppSettings appSettings = new AppSettings();
            services.AddSingleton(appSettings);
        }

        protected void AddSmtpEmailconfiguration(IServiceCollection services)
        {
            SmtpConfiguration smtpConfig = new SmtpConfiguration();
            services.AddSingleton(smtpConfig);
        }

        protected void AddMailer(ServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IEmailService, SmtpEmailService>();
            serviceCollection.AddTransient<IMailTransport, SmtpClient>();
        }

        protected void AddEntities<TEntity>(params TEntity[] entities) where TEntity : class, IEntity
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.AddRange(entities);
        }

        protected void Equivalent(object expected, object atual)
        {
            var expectedStr = JsonConvert.SerializeObject(expected);
            var actualStr = JsonConvert.SerializeObject(atual);

            Assert.Equal(expectedStr, actualStr);
        }
    }
}
