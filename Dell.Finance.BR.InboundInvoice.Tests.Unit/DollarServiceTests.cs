﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Internal;
using NSubstitute;
using System;
using System.Collections.Generic;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Unit
{
    public class DollarServiceTests : UnitTestBase
    {
        private readonly IDollarRateService _dollarRateService;

        public DollarServiceTests()
        {
            _dollarRateService = ServiceProvider.GetService<IDollarRateService>();
        }

        [Fact]
        public void GetDollarRate_Should_ReturnAValue()
        {
            //Arrange
            var expectedDollarRate = 4;
            SeedInvoiceDollarRate(expectedDollarRate);

            //Act
            var dollarRate = _dollarRateService.GetDollarRate();

            //Assert
            Assert.Equal(expectedDollarRate, dollarRate);
        }

        private void SeedInvoiceDollarRate(decimal dollarRate)
        {
            var systemClock = Substitute.For<ISystemClock>();
            systemClock.UtcNow.Returns(DateTime.Now.AddDays(-10));

            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Invoice()
            {
                TxConversaoMoedaNac = dollarRate,
                DtFatoGeradorImposto = systemClock.UtcNow.Date,
                SiglaMoeda = DollarRateService.SIGLA_MOEDA_USD,
                SisCodigo = DollarRateService.SIS_CODIGO_EDI,
            });
        }
    }
}
