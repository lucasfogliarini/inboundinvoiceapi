﻿using Dell.Finance.BR.InboundInvoice.Core.Services.Contracts;
using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Unit
{
    public sealed class InboundInvoiceIssueQueueServiceTests : UnitTestBase
    {
        private readonly IInboundInvoiceIssueQueueService inboundInvoiceIssueQueueService;

        public InboundInvoiceIssueQueueServiceTests()
        {
            this.inboundInvoiceIssueQueueService = ServiceProvider.GetService<IInboundInvoiceIssueQueueService>();
        }

        [Fact]
        public void GetByOutboundIdShouldNotBeNull()
        {
            // Arrange
            SeedInboundInvoiceIssueQueue();

            // Act
            var inboundInvoiceIssueQueue = this.inboundInvoiceIssueQueueService.GetByOutboundId(456);

            // Assert
            Assert.NotNull(inboundInvoiceIssueQueue);
        }

        [Fact]
        public void UpdateShouldBeEqual()
        {
            // Arrange
            SeedInboundInvoiceIssueQueue();

            var inboundInvoiceIssueQueue = new InboundInvoiceIssueQueue
            {
                CreateDate = DateTime.Now,
                Id = "1",
                InboundDofId = 123,
                OutboundDofId = 456,
                ProcessStatus = Infrastructure.Enums.InboundInvoiceProcessStatus.InboundInvoiceAuthorized,
                ProcessType = Infrastructure.Enums.InboundInvoiceProcessType.DellIFG,
                UpdatedDate = DateTime.Now,
                LastEmailSent = DateTime.Now
            };

            // Act
            this.inboundInvoiceIssueQueueService.UpdateInboundInvoiceIssueQueue(inboundInvoiceIssueQueue);

            var updatedInboundInvoice = this.inboundInvoiceIssueQueueService.GetByOutboundId(456);

            // Assert
            Assert.Equal(inboundInvoiceIssueQueue.LastEmailSent, updatedInboundInvoice.LastEmailSent);
        }

        private void SeedInboundInvoiceIssueQueue()
        {
            var listValues = new List<InboundInvoiceIssueQueue>()
            {
                new InboundInvoiceIssueQueue
                {
                    CreateDate = DateTime.Now,
                    Id = "1",
                    InboundDofId = 123,
                    OutboundDofId = 456,
                    ProcessStatus = Infrastructure.Enums.InboundInvoiceProcessStatus.InboundInvoiceAuthorized,
                    ProcessType = Infrastructure.Enums.InboundInvoiceProcessType.DellIFG,
                    UpdatedDate = DateTime.Now
                },
                new InboundInvoiceIssueQueue
                {
                    CreateDate = DateTime.Now,
                    Id = "2",
                    InboundDofId = 123123,
                    LastEmailSent = DateTime.Now,
                    OutboundDofId = 456456,
                    ProcessStatus = Infrastructure.Enums.InboundInvoiceProcessStatus.InboundInvoiceAuthorized,
                    ProcessType = Infrastructure.Enums.InboundInvoiceProcessType.DellIFG,
                    UpdatedDate = DateTime.Now
                }
            };
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.AddRange(listValues);
        }
    }
}
