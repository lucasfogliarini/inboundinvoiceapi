﻿using Dell.Finance.BR.InboundInvoice.Core.Services.Contracts;
using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Unit
{
    public sealed class InvoiceItemDataServiceTests : UnitTestBase
    {
        private readonly IInvoiceItemDataService invoiceItemDataService;

        public InvoiceItemDataServiceTests()
        {
            this.invoiceItemDataService = ServiceProvider.GetService<IInvoiceItemDataService>();
        }

        [Fact]
        public void GetIpOwnerShouldNotBeEmpytOrNull()
        {
            SeedInvoiceItemData();

            var ipOwner = this.invoiceItemDataService.GetIpOwner(1, 00002, 00001);

            Assert.NotNull(ipOwner);
        }

        [Fact]
        public void GetIpOwnerShouldBeEquals()
        {
            SeedInvoiceItemData();

            var ipOwner = this.invoiceItemDataService.GetIpOwner(1, 00003, 00001);
            var expectedIpOwner = "EMC_VMWARE";

            Assert.Equal(expectedIpOwner, ipOwner);
        }

        [Fact]
        public void GetIpOwnerShouldBeEmpty()
        {
            SeedInvoiceItemData();

            var ipOwner = this.invoiceItemDataService.GetIpOwner(1, 00001, 00001);

            Assert.Empty(ipOwner);
        }

        [Fact]
        public void GetIpOwnerShouldThrowException()
        {
            SeedInvoiceItemData();

            Assert.Throws<ApplicationException>(() => this.invoiceItemDataService.GetIpOwner(1, 00005, 00001));
        }

        private void SeedInvoiceItemData()
        {
            var listValues = new List<InvoiceItemData>()
            {
                new InvoiceItemData()
                {
                    CodigoDoSite = 1,
                    DofSequence = 00001,
                    IdfNum = 00001,
                    IpOwnerAttribute = ""
                },
                new InvoiceItemData()
                {
                    CodigoDoSite = 1,
                    DofSequence = 00002,
                    IdfNum = 00001,
                    IpOwnerAttribute = "DELL"
                },
                new InvoiceItemData()
                {
                    CodigoDoSite = 1,
                    DofSequence = 00003,
                    IdfNum = 00001,
                    IpOwnerAttribute = "EMC_VMWARE"
                },
                new InvoiceItemData()
                {
                    CodigoDoSite = 1,
                    DofSequence = 00004,
                    IdfNum = 00001,
                    IpOwnerAttribute = "EMC"
                }
            };
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.AddRange(listValues);
        }
    }
}
