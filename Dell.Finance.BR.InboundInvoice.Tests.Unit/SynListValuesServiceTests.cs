﻿using Dell.Finance.BR.InboundInvoice.Core.Services.Contracts;
using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Unit
{
    public sealed class SynListValuesServiceTests : UnitTestBase
    {
        private readonly ISynListValuesService synListValuesService;

        public SynListValuesServiceTests()
        {
            this.synListValuesService = ServiceProvider.GetService<ISynListValuesService>();
        }

        [Fact]
        public void GetPfjCodeShouldNotBeEmpytOrNull()
        {
            // Arrange
            SeedSynListValues();
             
            // Act
            var pfjCode = this.synListValuesService.GetSenderPfjCode("EMPTY", new Invoice());

            // Assert
            Assert.NotNull(pfjCode);
        }

        [Fact]
        public void GetPfjCodeShouldBeEquals()
        {
            // Arrange
            SeedSynListValues();
            var expectedpfjCode = "EMCF25";

            // Act
            var pfjCode = this.synListValuesService.GetSenderPfjCode("EMC_VMWARE", new Invoice());

            // Assert
            Assert.Equal(expectedpfjCode, pfjCode);
        }

        [Fact]
        public void GetPfjCodeShouldThrowException()
        {
            // Arrange
            SeedSynListValues();

            // Act
            Action act = () => this.synListValuesService.GetSenderPfjCode("TESTE", new Invoice());

            // Assert
            Assert.Throws<ApplicationException>(act);
        }

        private void SeedSynListValues()
        {
            var listValues = new List<SynListValues>()
            {
                new SynListValues
                {
                    ListaTipo = "IP_OWNER_LIST",
                    ListaValor = "DELL",
                    Description = "67117F"
                },
                new SynListValues
                {
                    ListaTipo = "IP_OWNER_LIST",
                    ListaValor = "EMC",
                    Description = "EMCF25"
                },
                new SynListValues
                {
                    ListaTipo = "IP_OWNER_LIST",
                    ListaValor = "EMC_VMWARE",
                    Description = "EMCF25"
                },
                new SynListValues
                {
                    ListaTipo = "IP_OWNER_LIST",
                    ListaValor = "EMPTY",
                    Description = "67117F",
                }
            };

            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.AddRange(listValues);
        }
    }
}
