﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Internal;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Unit
{
    public class InboundInvoiceSNPServiceTests : UnitTestBase
    {
        private readonly IInboundInvoiceSNPService _inboundInvoiceService;

        public InboundInvoiceSNPServiceTests()
        {
            _inboundInvoiceService = ServiceProvider.GetService<IInboundInvoiceSNPService>();
        }

        [Fact]
        public void GetOutboundWithItems_Should_Get_NotNull_Outbound()
        {
            var outboundId = 998;
            SeedInvoice(outboundId);

            Invoice outbound = _inboundInvoiceService.GetOutboundWithItems(outboundId);

            Assert.NotNull(outbound);
        }

        [Fact]
        public void CloneItemsAndCalculateItemPrice_Should_CloneAndCalculate()
        {
            SeedSynParameter();
            SeedSynListValues();
            SeedInvoiceDollarRate(4);
            var outbound = new Invoice()
            {
                Items = new List<InvoiceItem>()
                {
                    GetInvoiceItem(1, 3.5m, 2),
                    GetInvoiceItem(2, 35.8m, 0),
                    GetInvoiceItem(3, 17.5m, 0),
                }
            };
            var newInbound = new Invoice()
            {
                DofSequence = 123,
                CodigoDoSite = ImportedInboundInvoiceService.CODIGO_DO_SITE,
            };

            _inboundInvoiceService.CloneItemsAndCalculateItemUnitPrice(newInbound, outbound);

            Assert.Equal(outbound.Items.Where(e=>e.Qtd > 0).Count(), newInbound.Items.Count);
        }

        [Fact]
        public void CalculateInboundItemPrice_Should_Calculate()
        {
            var dollarRate = 4;
            var markupPercentage = 0.2m;
            var outboundItemUnitPriceUSD = 25.9m;

            var inboundItemPrice = _inboundInvoiceService.CalculateInboundItemUnitPrice(outboundItemUnitPriceUSD, dollarRate, markupPercentage);

            var expectedInboundItemPrice = 82.88m;
            Assert.Equal(expectedInboundItemPrice, inboundItemPrice);
        }

        [Fact]
        public void GetMarkupPercentage_Should_Get()
        {
            SeedSynParameter();
            SeedSynListValues();
            var pfjCodigo = "321";

            var markupPercentage = _inboundInvoiceService.GetMarkupPercentage(pfjCodigo);

            Assert.Equal(0.2m, markupPercentage);
        }

        [Fact]
        public void GetMarkupPercentage_Should_Throw_Exception()
        {
            SeedSynParameter();
            SeedSynListValues();
            var pfjCodigo = "3453425";

            Assert.Throws<ApplicationException>(() =>
            {
                _inboundInvoiceService.GetMarkupPercentage(pfjCodigo);
            });
        }

        private void SeedInvoiceDollarRate(decimal dollarRate)
        {
            var systemClock = Substitute.For<ISystemClock>();
            systemClock.UtcNow.Returns(DateTime.Now.AddDays(-10));

            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Invoice()
            {
                TxConversaoMoedaNac = dollarRate,
                SiglaMoeda = DollarRateService.SIGLA_MOEDA_USD,
                SisCodigo = DollarRateService.SIS_CODIGO_EDI,
                DtFatoGeradorImposto = DateTime.Now
            });
        }

        private void SeedInvoice(long invoiceid)
        {
            var systemClock = Substitute.For<ISystemClock>();
            systemClock.UtcNow.Returns(DateTime.Now.AddDays(-10));

            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Invoice()
            {
                Id = invoiceid,
                IndEntradaSaida = "S",
                EmitentePfjCodigo = "Emitente123",
                RemetentePfjCodigo = "Remetente123",
                DestinatarioPfjCodigo = "Destinatario123",
                DlbRecofNumOrder = "NumeroOrdem123",
                DellUsOrder = "123456789",//9chars
                DtFatoGeradorImposto = systemClock.UtcNow.Date,
                SiglaMoeda = "USD",
                SisCodigo = "EDI",
                TxConversaoMoedaNac = 4,
                Items = new List<InvoiceItem>()
                {
                    new InvoiceItem()
                    {
                       MercCodigo = "MercCodigo1",
                       Qtd = 3,
                       InvoiceItemData = new InvoiceItemData()
                       {
                           FulfillmentLocId = "00",
                           PrecoUnitarioUSD = 13.5m
                       }
                    }
                }
            });
        }

        private void SeedSynParameter()
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new SynParameter()
            {
                ParCodigo = InboundInvoiceSNPService.MICROSOFT_SNP_MARKUP,
                Valor = "20"
            });
            unitOfWork.Add(new SynParameter()
            {
                ParCodigo = InboundInvoiceSNPService.VMWARE_SNP_MARKUP,
                Valor = "10"
            });
        }

        private void SeedSynListValues()
        {
            var listValues = new List<SynListValues>()
            {
                new SynListValues()
                {
                    ListaTipo = "VMWARE_SNP_PFJ_CODIGOS",
                    ListaValor = "123"
                },
                new SynListValues()
                {
                    ListaTipo = "VMWARE_SNP_PFJ_CODIGOS",
                    ListaValor = "456"
                },
                new SynListValues()
                {
                    ListaTipo = "MICROSOFT_SNP_PFJ_CODIGOS",
                    ListaValor = "321"
                },
                new SynListValues()
                {
                    ListaTipo = "MICROSOFT_SNP_PFJ_CODIGOS",
                    ListaValor = "544346F"
                }
            };
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.AddRange(listValues);
        }
        private InvoiceItem GetInvoiceItem(long idfNum, decimal itemPriceUSD, int quantity)
        {
            return new InvoiceItem()
            {
                CodigoDoSite = ImportedInboundInvoiceService.CODIGO_DO_SITE,
                DofSequence = 123,
                IdfNum = idfNum,
                MercCodigo = "MERC123",
                Qtd = quantity,
                InvoiceItemData = new InvoiceItemData()
                {
                    PrecoUnitarioUSD = itemPriceUSD
                }
            };
        }
    }
}
