﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Unit
{
    public class ImportedInboundInvoiceServiceTests : UnitTestBase
    {
        private readonly IInboundInvoiceEMCService _inboundInvoiceService;

        public ImportedInboundInvoiceServiceTests()
        {
            _inboundInvoiceService = ServiceProvider.GetService<IInboundInvoiceEMCService>();
        }

        [Fact]
        public void SetInvoiceExchangeDeclaration_Should_Set_Declaration()
        {
            var inbound = new Invoice()
            {
                CodigoDoSite = ImportedInboundInvoiceService.CODIGO_DO_SITE,
                DofSequence = 123,
                RemetentePfjCodigo = "REMETENTE123",
                Items = new List<InvoiceItem>()
                {
                    new InvoiceItem()
                    {
                        IdfNum = 1
                    }
                }
            };

            _inboundInvoiceService.SetInvoiceExchangeDeclaration(inbound);

            Assert.True(true);
        }

        [Fact]
        public void GetRelatedInboundId_Should_GetRelatedInboundId()
        {
            var recofNumOrder = "order1";
            var expectedRelatedInbound = new Invoice()
            {
                Id = 999,
                IndEntradaSaida = ImportedInboundInvoiceService.ENTRADA,
                DlbRecofNumOrder = recofNumOrder
            };
            AddEntities(expectedRelatedInbound);

            var relatedInboundId = _inboundInvoiceService.GetRelatedInboundId(recofNumOrder);

            Assert.Equal(expectedRelatedInbound.Id, relatedInboundId);
        }
        [Fact]
        public void GetRelatedInboundId_Should_ReturnNull()
        {
            var recofNumOrder = "order1";

            var relatedInboundId = _inboundInvoiceService.GetRelatedInboundId(recofNumOrder);

            Assert.Null(relatedInboundId);
        }

        [Fact]
        public void GetRelatedInboundId_Should_ReturnException()
        {
            var recofNumOrder = "duplicate_order_number";
            var inbound1 = new Invoice()
            {
                Id = 1,
                IndEntradaSaida = ImportedInboundInvoiceService.ENTRADA,
                DlbRecofNumOrder = recofNumOrder
            };
            var inbound2 = new Invoice()
            {
                Id = 2,
                IndEntradaSaida = ImportedInboundInvoiceService.ENTRADA,
                DlbRecofNumOrder = recofNumOrder
            };
            AddEntities(inbound1, inbound2);

            Assert.Throws<ApplicationException>(() =>
            {
                _inboundInvoiceService.GetRelatedInboundId(recofNumOrder);
            });
        }
    }
}
