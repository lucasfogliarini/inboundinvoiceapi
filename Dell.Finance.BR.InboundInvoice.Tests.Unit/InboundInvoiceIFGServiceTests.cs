﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Internal;
using NSubstitute;
using System;
using System.Collections.Generic;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Unit
{
    public class InboundInvoiceIFGServiceTests : UnitTestBase
    {
        private readonly IInboundInvoiceIFGService _inboundInvoiceService;

        public InboundInvoiceIFGServiceTests()
        {
            _inboundInvoiceService = ServiceProvider.GetService<IInboundInvoiceIFGService>();
        }

        [Fact]
        public void GetOutboundWithItems_Should_Get_NotNull_Outbound()
        {
            var outboundId = 998;
            SeedInvoice(outboundId);

            Invoice outbound = _inboundInvoiceService.GetOutboundWithItems(outboundId);

            Assert.NotNull(outbound);
        }

        [Fact]
        public void CalculateInboundItemPrice_Should_Calculate()
        {
            var outboundItems = new List<InvoiceItem>()
            {
                GetInvoiceItem(1, 3.5m, 2, true),
                GetInvoiceItem(2, 5.8m, 0, true),
                GetInvoiceItem(3, 17.5m, 0, false),
            };
            var dollarRate = 4;
            SeedInvoiceDollarRate(dollarRate);

            var inboundItemPrice = _inboundInvoiceService.CalculateInboundItemPrice(outboundItems);

            var expectedInboundItemPrice = 9.3M * dollarRate;
            Assert.Equal(expectedInboundItemPrice, inboundItemPrice);
        }

        [Fact]
        public void CloneItemAndCalculateItemPrice_Should_CloneAndCalculate()
        {
            var outbound = new Invoice()
            {
                DlbRecofNumOrder = "DlbRecofNumOrder1",
                Items =  new List<InvoiceItem>()
                {
                    GetInvoiceItem(1, 3.5m, 2, true),
                    GetInvoiceItem(2, 35.8m, 0, true),
                    GetInvoiceItem(3, 17.5m, 0, false),
                }
            };
            var dollarRate = 4;
            SeedInvoiceDollarRate(dollarRate);

            var newInbound = new Invoice()
            {
                DofSequence = 123,
                CodigoDoSite = ImportedInboundInvoiceService.CODIGO_DO_SITE,
            };

            _inboundInvoiceService.CloneItemAndCalculateItemPrice(newInbound, outbound);

            var expectedItemsCount = 1;//only qtd > 0
            Assert.Equal(expectedItemsCount, newInbound.Items.Count);
        }

        private void SeedInvoiceDollarRate(decimal dollarRate)
        {
            var systemClock = Substitute.For<ISystemClock>();
            systemClock.UtcNow.Returns(DateTime.Now.AddDays(-10));

            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Invoice()
            {
                TxConversaoMoedaNac = dollarRate,
                DtFatoGeradorImposto = systemClock.UtcNow.Date,
                SiglaMoeda = DollarRateService.SIGLA_MOEDA_USD,
                SisCodigo = DollarRateService.SIS_CODIGO_EDI,
            });
        }

        private void SeedInvoice(long invoiceid)
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Invoice()
            {
                Id = invoiceid,
                IndEntradaSaida = "S",
                EmitentePfjCodigo = "Emitente123",
                RemetentePfjCodigo = "Remetente123",
                DestinatarioPfjCodigo = "Destinatario123",
                DlbRecofNumOrder = "NumeroOrdem123",
                DellUsOrder = "DellUsOrder123",
                Items = new List<InvoiceItem>()
                {
                    new InvoiceItem()
                    {
                       MercCodigo = "MercCodigo1",
                       Qtd = 3,
                       InvoiceItemData = new InvoiceItemData()
                       {
                           FulfillmentLocId = "00",
                           PrecoUnitarioUSD = 13.5m
                       }
                    }
                }
            });
        }

        private InvoiceItem GetInvoiceItem(long idfNum, decimal itemPriceUSD, int quantity, bool fullfilment)
        {
            return new InvoiceItem()
            {
                CodigoDoSite = ImportedInboundInvoiceService.CODIGO_DO_SITE,
                DofSequence = 123,
                IdfNum = idfNum,
                MercCodigo = "MERC123",
                Qtd = quantity,
                InvoiceItemData = new InvoiceItemData()
                {
                    PrecoUnitarioUSD = itemPriceUSD,
                    FulfillmentLocId = fullfilment ? "00" : ""
                }
            };
        }
    }
}
