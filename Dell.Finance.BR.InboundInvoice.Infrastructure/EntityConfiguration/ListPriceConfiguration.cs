﻿using Dell.Finance.BR.InboundInvoice.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure.EntityConfiguration
{
    internal sealed class ListPriceConfiguration : IEntityTypeConfiguration<ListPrice>
    {
        public void Configure(EntityTypeBuilder<ListPrice> builder)
        {            
            builder.ToTable("PRODUCT_LIST_PRICE", "LATAM_FINANCE");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).HasColumnName("ID").IsRequired();
            builder.Property(c => c.ItemCode).HasColumnName("ITEM_CODE").IsRequired();
            builder.Property(c => c.Price).HasColumnName("PRICE").IsRequired();
        }
    }
}
