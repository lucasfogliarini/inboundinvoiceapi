﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    internal class SynParameterConfiguration : IEntityTypeConfiguration<SynParameter>
    {
        public void Configure(EntityTypeBuilder<SynParameter> builder)
        {
            builder.ToTable("SYN_PARAMETROS");
            builder.HasKey(e => e.ParCodigo);
            builder.Property(c => c.ParCodigo).HasColumnName("PAR_CODIGO").IsRequired();
            builder.Property(c => c.Valor).HasColumnName("VALOR").IsRequired();
        }
    }
}
