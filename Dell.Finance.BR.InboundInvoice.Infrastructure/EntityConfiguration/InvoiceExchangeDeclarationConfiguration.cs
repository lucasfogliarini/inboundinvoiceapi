﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    internal class InvoiceExchangeDeclarationConfiguration : IEntityTypeConfiguration<InvoiceExchangeDeclaration>
    {
        public void Configure(EntityTypeBuilder<InvoiceExchangeDeclaration> builder)
        {
            builder.ToTable("COR_IMP_EXP");
            builder.HasKey(e => e.Id);
            builder.Property(c => c.Id).HasColumnName("ID");
            builder.Property(c => c.CodigoDoSite).HasColumnName("CODIGO_DO_SITE").IsRequired();
            builder.Property(c => c.DofSequence).HasColumnName("DOF_SEQUENCE").IsRequired();
            builder.Property(c => c.NumDeclImpExp).HasColumnName("NUM_DECL_IMPEXP").IsRequired();
            builder.Property(c => c.NumRegExport).HasColumnName("NUM_REG_EXPORT").IsRequired();
            builder.Property(c => c.NfeLocalizador).HasColumnName("NFE_LOCALIZADOR").IsRequired();
            builder.Property(c => c.DtEmiReDi).HasColumnName("DT_EMI_RE_DI");
            builder.Property(c => c.DtRegExportacao).HasColumnName("DT_RG_EXPORTACAO");
            builder.Property(c => c.DtDeclaracao).HasColumnName("DT_DECLARACAO");
            builder.Property(c => c.FormaImportacao).HasColumnName("FORMA_IMPORTACAO").IsRequired();
            builder.Property(c => c.ExportadorPfjCodigo).HasColumnName("EXPORTADOR_PFJ_CODIGO").IsRequired();
            builder.Property(c => c.ViaTransporte).HasColumnName("VIA_TRANSPORTE").IsRequired();
            builder.Property(c => c.VlAfrmm).HasColumnName("VL_AFRMM").IsRequired();
            builder.Property(c => c.DesembaracoLoc).HasColumnName("DESEMBARACO_LOC").IsRequired();
            builder.Property(c => c.DesembaracoUf).HasColumnName("DESEMBARACO_UF").IsRequired();
            builder.Property(c => c.DesembaracoData).HasColumnName("DESEMBARACO_DATA");
        }
    }
}
