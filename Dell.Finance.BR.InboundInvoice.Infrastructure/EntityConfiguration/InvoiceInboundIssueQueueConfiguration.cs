﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public sealed class InboundInvoiceIssueQueueConfiguration : IEntityTypeConfiguration<InboundInvoiceIssueQueue>
    {
        public void Configure(EntityTypeBuilder<InboundInvoiceIssueQueue> builder)
        {
            builder.ToTable("INBOUND_NF_ISSUE_QUEUE", "LATAM_FINANCE");
            builder.HasKey(c => new { c.Id });                        
            builder.Property(c => c.Id).HasColumnName("ID").IsRequired();
            builder.Property(c => c.OutboundDofId).HasColumnName("OUTBOUND_DOFID").IsRequired();
            builder.Property(c => c.InboundDofId).HasColumnName("INBOUND_DOFID");
            builder.Property(c => c.ProcessStatus).HasColumnName("PROCESS_STATUS").IsRequired();
            builder.Property(c => c.ProcessType).HasColumnName("PROCESS_TYPE").IsRequired();
            builder.Property(c => c.CreateDate).HasColumnName("CREATE_DATE").IsRequired();
            builder.Property(c => c.UpdatedDate).HasColumnName("UPDATED_DATE").IsRequired();
            builder.Property(c => c.LastEmailSent).HasColumnName("LAST_EMAIL_SENT");
        }
    }
}
