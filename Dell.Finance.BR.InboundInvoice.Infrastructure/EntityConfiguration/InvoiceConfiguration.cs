﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    internal class InvoiceConfiguration : IEntityTypeConfiguration<Invoice>
    {
        public void Configure(EntityTypeBuilder<Invoice> builder)
        {
            builder.ToTable("COR_DOF");
            builder.HasKey(c => new { c.DofSequence, c.CodigoDoSite });
            builder.Property(c => c.Id).HasColumnName("ID").IsRequired();
            builder.Property(c => c.DofSequence).HasColumnName("DOF_SEQUENCE").IsRequired();
            builder.Property(c => c.CodigoDoSite).HasColumnName("CODIGO_DO_SITE").IsRequired();
            builder.Property(c => c.DofImportNumero).HasColumnName("DOF_IMPORT_NUMERO").IsRequired();
            builder.Property(c => c.SisCodigo).HasColumnName("SIS_CODIGO").IsRequired();
            builder.Property(c => c.DlbRecofNumOrder).HasColumnName("DELL_RECOF_NUM_ORDER");
            builder.Property(c => c.DellUsOrder).HasColumnName("DELL_US_ORDER");
            builder.Property(c => c.DlbNumeroOrdemCompra).HasColumnName("DLB_NUMERO_ORDEM_COMPRA");
            builder.Property(c => c.ModoEmissao).HasColumnName("MODO_EMISSAO").IsRequired();
            builder.Property(c => c.CtrlConteudo).HasColumnName("CTRL_CONTEUDO").IsRequired();
            builder.Property(c => c.IndEix).HasColumnName("IND_EIX").IsRequired();
            builder.Property(c => c.IndEntradaSaida).HasColumnName("IND_ENTRADA_SAIDA").IsRequired();
            builder.Property(c => c.Tipo).HasColumnName("TIPO");
            builder.Property(c => c.EmitentePfjCodigo).HasColumnName("EMITENTE_PFJ_CODIGO");
            builder.Property(c => c.InformanteEstCodigo).HasColumnName("INFORMANTE_EST_CODIGO");
            builder.Property(c => c.RemetentePfjCodigo).HasColumnName("REMETENTE_PFJ_CODIGO");
            builder.Property(c => c.DestinatarioPfjCodigo).HasColumnName("DESTINATARIO_PFJ_CODIGO");
            builder.Property(c => c.MdofCode).HasColumnName("MDOF_CODIGO");
            builder.Property(c => c.EdofCode).HasColumnName("EDOF_CODIGO");
            builder.Property(c => c.Serie).HasColumnName("SERIE_SUBSERIE");
            builder.Property(c => c.CfopCodigo).HasColumnName("CFOP_CODIGO");
            builder.Property(c => c.NopCodigo).HasColumnName("NOP_CODIGO");
            builder.Property(c => c.PesoLiquido).HasColumnName("PESO_LIQUIDO_KG");
            builder.Property(c => c.PesoBruto).HasColumnName("PESO_BRUTO_KG");
            builder.Property(c => c.IndRespFrete).HasColumnName("IND_RESP_FRETE");
            builder.Property(c => c.DtFatoGeradorImposto).HasColumnName("DT_FATO_GERADOR_IMPOSTO").HasColumnType("date");
            builder.Property(c => c.SiglaMoeda).HasColumnName("SIGLA_MOEDA");
            builder.Property(c => c.TxConversaoMoedaNac).HasColumnName("TX_CONVERSAO_MOEDA_NAC");
            builder.HasMany(e => e.Items).WithOne(e => e.Invoice).HasForeignKey(e=>new { e.DofSequence, e.CodigoDoSite });
        }
    }
}
