﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    internal class InvoiceItemConfiguration : IEntityTypeConfiguration<InvoiceItem>
    {
        public void Configure(EntityTypeBuilder<InvoiceItem> builder)
        {
            builder.ToTable("COR_IDF");
            builder.HasKey(e => new { e.IdfNum, e.DofSequence, e.CodigoDoSite});
            builder.Property(c => c.Id).HasColumnName("ID").IsRequired();
            builder.Property(c => c.IdfNum).HasColumnName("IDF_NUM").IsRequired();
            builder.Property(c => c.DofSequence).HasColumnName("DOF_SEQUENCE").IsRequired();
            builder.Property(c => c.CodigoDoSite).HasColumnName("CODIGO_DO_SITE").IsRequired();
            builder.Property(c => c.MercCodigo).HasColumnName("MERC_CODIGO").IsRequired();
            builder.Property(c => c.Qtd).HasColumnName("QTD").IsRequired();
            builder.Property(c => c.PrecoUnitario).HasColumnName("PRECO_UNITARIO").IsRequired();
            builder.Property(c => c.PrecoTotal).HasColumnName("PRECO_TOTAL").IsRequired();
            builder.Property(c => c.DlbLaDomsOrder).HasColumnName("DLB_LA_DOMS_ORDER");
            builder.HasOne(e => e.InvoiceItemData).WithOne(e => e.InvoiceItem).HasForeignKey<InvoiceItem>(e => new { e.IdfNum, e.DofSequence, e.CodigoDoSite });//must be same pk order
        }
    }
}
