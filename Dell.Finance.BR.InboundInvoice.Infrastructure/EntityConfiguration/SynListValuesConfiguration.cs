﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    internal class SynListValuesConfiguration : IEntityTypeConfiguration<SynListValues>
    {
        public void Configure(EntityTypeBuilder<SynListValues> builder)
        {
            builder.ToTable("SYN_LISTA_VALORES");
            builder.HasKey(e => new { e.ListaTipo, e.ListaValor });
            builder.Property(c => c.Description).HasColumnName("DESCRICAO").IsRequired();
            builder.Property(c => c.ListaTipo).HasColumnName("LISTA_TIPO").IsRequired();
            builder.Property(c => c.ListaValor).HasColumnName("LISTA_VALOR").IsRequired();
        }
    }
}
