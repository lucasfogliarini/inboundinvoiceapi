﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    internal class InvoiceExchangeDeclarationItemConfiguration : IEntityTypeConfiguration<InvoiceExchangeDeclarationItem>
    {
        public void Configure(EntityTypeBuilder<InvoiceExchangeDeclarationItem> builder)
        {
            builder.ToTable("COR_IMP_EXP_ADIC");
            builder.HasKey(e => new { e.IdfNum, e.CieId, e.NumSeqAdicao, e.NumAdicao });
            builder.Property(c => c.CodigoDoSite).HasColumnName("CODIGO_DO_SITE");
            builder.Property(c => c.DofSequence).HasColumnName("DOF_SEQUENCE");
            builder.Property(c => c.NumAdicao).HasColumnName("NUM_ADICAO").IsRequired();
            builder.Property(c => c.NumSeqAdicao).HasColumnName("NUM_SEQ_ADICAO").IsRequired();
            builder.Property(c => c.FabricantePfjCodigo).HasColumnName("FABRICANTE_PFJ_CODIGO");
            builder.Property(c => c.DescontoAdicao).HasColumnName("DESCONTO_ADICAO");
            builder.Property(c => c.NumDrawback).HasColumnName("NUM_DRAWBACK");
            builder.Property(c => c.IdfNum).HasColumnName("IDF_NUM").IsRequired();
            builder.Property(c => c.NumDeclImpExp).HasColumnName("NUM_DECL_IMPEXP");
            builder.Property(c => c.CieId).HasColumnName("CIE_ID").IsRequired();
            builder.Property(c => c.NumRegExport).HasColumnName("NUM_REG_EXPORT");
            builder.Property(c => c.NumRegExportNew).HasColumnName("NUM_REG_EXPORT_NEW");
        }
    }
}
