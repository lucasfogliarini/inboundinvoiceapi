﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    internal class InvoiceItemDataConfiguration : IEntityTypeConfiguration<InvoiceItemData>
    {
        public void Configure(EntityTypeBuilder<InvoiceItemData> builder)
        {
            builder.ToTable("DLB_SALES_ITEM_DATA");
            builder.HasKey(e => new { e.IdfNum, e.DofSequence, e.CodigoDoSite });
            builder.Property(t => t.CodigoDoSite).HasColumnName("CODIGO_DO_SITE").IsRequired();
            builder.Property(t => t.DofSequence).HasColumnName("DOF_SEQUENCE").IsRequired();
            builder.Property(t => t.IdfNum).HasColumnName("IDF_NUM").IsRequired();
            builder.Property(t => t.PrecoUnitarioUSD).HasColumnName("ITEM_UNIT_COST_AMOUNT_USD");
            builder.Property(t => t.PrecoUnitario).HasColumnName("ITEM_UNIT_COST_AMOUNT");
            builder.Property(t => t.FulfillmentLocId).HasColumnName("FULFILLMENT_LOC_ID");
            builder.Property(t => t.IpOwnerAttribute).HasColumnName("IP_OWNER_ATTRIBUTE");
            builder.HasOne(e => e.InvoiceItem).WithOne(e => e.InvoiceItemData).HasForeignKey<InvoiceItemData>(e=> new { e.IdfNum, e.DofSequence, e.CodigoDoSite });//must be same pk order
        }
    }
}
