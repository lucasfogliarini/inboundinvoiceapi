﻿using Dell.Finance.BR.InboundInvoice.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSynchroUnitOfWork(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<SynchroDbContext>(o => o.UseOracle(connectionString, opt => {
                opt.UseOracleSQLCompatibility("11");
            }));
            services.AddScoped<IUnitOfWork, SynchroUnitOfWork>();
            return services;
        }
        public static IServiceCollection AddFakeUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, FakeUnitOfWork>();
            return services;
        }
    }
}