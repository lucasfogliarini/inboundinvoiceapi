﻿namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public class InvoiceItemData : IEntity
    {
        public long CodigoDoSite { get; set; }
        public long DofSequence { get; set; }
        public long IdfNum { get; set; }
        public decimal? PrecoUnitarioUSD { get; set; }
        public decimal? PrecoUnitario { get; set; }
        public string FulfillmentLocId { get; set; }
        public InvoiceItem InvoiceItem { get; set; }
        public string IpOwnerAttribute { get; set; }
    }
}