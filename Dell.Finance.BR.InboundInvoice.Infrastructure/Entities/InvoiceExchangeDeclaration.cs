﻿using System;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public class InvoiceExchangeDeclaration : IEntity
    {
        public long Id { get; set; }
        public long CodigoDoSite { get; set; }
        public long DofSequence { get; set; }
        public string NumDeclImpExp { get; set; }
        public string NumRegExport { get; set; }
        public string NfeLocalizador { get; set; }
        public DateTime? DtEmiReDi { get; set; }
        public DateTime? DtRegExportacao { get; set; }
        public DateTime? DtDeclaracao { get; set; }
        public string FormaImportacao { get; set; }
        public string ExportadorPfjCodigo { get; set; }
        public string ViaTransporte { get; set; }
        public decimal? VlAfrmm { get; set; }
        public string DesembaracoLoc { get; set; }
        public string DesembaracoUf { get; set; }
        public DateTime? DesembaracoData { get; set; }
    }
}