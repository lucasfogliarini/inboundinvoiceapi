﻿using Dell.Finance.BR.InboundInvoice.Infrastructure.Enums;
using System;
using System.Collections.Generic;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public sealed class InboundInvoiceIssueQueue : IEntity
    {
        public string Id { get; set; }

        public long OutboundDofId { get; set; }

        public long? InboundDofId { get; set; }

        public InboundInvoiceProcessStatus ProcessStatus { get; set; }

        public InboundInvoiceProcessType ProcessType { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime? LastEmailSent{ get; set; }
    }
}
