﻿using System;
using System.Collections.Generic;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public class Invoice : IEntity
    {
        public long Id { get; set; }
        public long CodigoDoSite { get; set; }
        public long DofSequence { get; set; }
        public string DofImportNumero { get; set; }
        public string SisCodigo { get; set; }
        public string ModoEmissao { get; set; }
        public string CtrlConteudo { get; set; }
        public string IndEix { get; set; }
        public string IndEntradaSaida { get; set; }
        public string Tipo { get; set; }
        public string InformanteEstCodigo { get; set; }
        public string EmitentePfjCodigo { get; set; }
        public string RemetentePfjCodigo { get; set; }
        public string DestinatarioPfjCodigo { get; set; }
        public string MdofCode { get; set; }
        public string EdofCode { get; set; }
        public string Serie { get; set; }
        public string CfopCodigo { get; set; }
        public string NopCodigo { get; set; }
        public string DlbNumeroOrdemCompra { get; set; }
        public string DlbRecofNumOrder { get; set; }
        public string DellUsOrder { get; set; }
        public decimal PesoLiquido { get; set; }
        public decimal PesoBruto { get; set; }
        public string IndRespFrete { get; set; }
        public IList<InvoiceItem> Items { get; set; }
        public DateTime? DtFatoGeradorImposto { get; set; }
        public string SiglaMoeda { get; set; }
        public decimal TxConversaoMoedaNac { get; set; }
    }
}