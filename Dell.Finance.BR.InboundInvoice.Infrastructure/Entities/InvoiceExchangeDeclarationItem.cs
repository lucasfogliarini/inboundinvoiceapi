﻿namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public class InvoiceExchangeDeclarationItem : IEntity
    {
        public long CodigoDoSite { get; set; }
        public long DofSequence { get; set; }
        public int NumAdicao { get; set; }
        public int NumSeqAdicao { get; set; }
        public string FabricantePfjCodigo { get; set; }
        public decimal DescontoAdicao { get; set; }
        public string NumDrawback { get; set; }
        public long IdfNum { get; set; }
        public string NumDeclImpExp { get; set; }
        public long CieId { get; set; }
        public string NumRegExport { get; set; }
        public string NumRegExportNew { get; set; }
    }
}