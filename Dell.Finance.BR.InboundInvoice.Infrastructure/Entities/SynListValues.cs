﻿namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public class SynListValues : IEntity
    {
        public string Description { get; set; }
        public string ListaTipo { get; set; }
        public string ListaValor { get; set; }
    }
}