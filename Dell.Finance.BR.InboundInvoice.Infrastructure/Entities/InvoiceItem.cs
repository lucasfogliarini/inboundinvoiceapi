﻿namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public class InvoiceItem : IEntity
    {
        public long Id { get; set; }
        public long IdfNum { get; set; }
        public long DofSequence { get; set; }
        public long CodigoDoSite { get; set; }
        public string MercCodigo { get; set; }
        public int Qtd { get; set; }
        public decimal PrecoUnitario { get; set; }
        public string DlbLaDomsOrder { get; set; }
        public decimal PrecoTotal { get; set; }
        public Invoice Invoice { get; set; }
        public InvoiceItemData InvoiceItemData { get; set; }
    }
}