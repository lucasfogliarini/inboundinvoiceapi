﻿namespace Dell.Finance.BR.InboundInvoice.Infrastructure.Entities
{
    public sealed class ListPrice : IEntity
    {
        public string Id { get; set; }

        public string ItemCode { get; set; }

        public decimal Price { get; set; }
    }
}
