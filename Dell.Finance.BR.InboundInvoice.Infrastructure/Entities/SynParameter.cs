﻿namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public class SynParameter : IEntity
    {
        public string ParCodigo { get; set; }
        public string Valor { get; set; }
    }
}