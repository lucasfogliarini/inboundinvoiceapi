﻿namespace Dell.Finance.BR.InboundInvoice.Infrastructure.Enums
{
    public enum InboundInvoiceProcessType
    {
        EMCImportedSoftwareResale = 1,
        DellIFG = 2,
        DellSNP = 3
    }
}
