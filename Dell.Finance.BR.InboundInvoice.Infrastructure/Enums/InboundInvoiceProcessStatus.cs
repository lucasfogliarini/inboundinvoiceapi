﻿namespace Dell.Finance.BR.InboundInvoice.Infrastructure.Enums
{
    public enum InboundInvoiceProcessStatus
    {
        OutboundInvoiceQueued = 0,
        InboundInvoiceCreated = 1,
        InboundInvoiceIssued = 2,
        InboundInvoiceAuthorized = 3,
        OutboundInvoiceIssued = 4,
        OutboundInvoiceAuthorized = 5
    }
}
