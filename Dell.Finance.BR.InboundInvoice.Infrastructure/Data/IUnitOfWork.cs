﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;
        void Add<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity;
        void Update<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void Commit();
        long GenerateSequence(string seqName);
        int ExecuteSqlCommand(string rawSqlString, params object[] parameters);
    }
}
