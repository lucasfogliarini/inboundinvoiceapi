﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    internal class SynchroUnitOfWork : IUnitOfWork
    {
        readonly DbContext _dbContext;
        public SynchroUnitOfWork(SynchroDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public long GenerateSequence(string seqName)
        {
            var dofSequence = ExecuteScalar($"SELECT {seqName}.NEXTVAL FROM DUAL");
            return Convert.ToInt64(dofSequence);
        }

        private object ExecuteScalar(string commandText)
        {
            var connection = _dbContext.Database.GetDbConnection();
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = commandText;
            var value = command.ExecuteScalar();
            connection.Close();
            return value;
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return _dbContext.Set<TEntity>().AsNoTracking();
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _dbContext.Add(entity);
        }

        public void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity
        {
            _dbContext.AddRange(entities);
        }

        public void Commit()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (Exception ex) when (ex.InnerException != null)
            {
                throw new ApplicationException(ex.InnerException.Message, ex);
            }
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _dbContext.Update(entity);
        }

        public int ExecuteSqlCommand(string rawSqlString, params object[] parameters)
        {
            return _dbContext.Database.ExecuteSqlCommand(rawSqlString, parameters);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        
    }
}
