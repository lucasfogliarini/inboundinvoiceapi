﻿namespace Dell.Finance.BR.InboundInvoice.Infrastructure.Data
{
    public class AppSettings
    {
        public string AuthKey { get; set; }
        public string ConnectionString { get; set; }
        public string LoggingProvider { get; set; }
        public string LoggingServerUrl { get; set; }
        public string LoggingServerName { get; set; }
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }

        public bool UseSeq()
        {
            return LoggingProvider.Contains("seq");
        }
        public bool UseOracleProvider()
        {
            return LoggingProvider.Contains("oracle_provider");
        }
    }
}
