﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure
{
    internal class SynchroDbContext : DbContext
    {
        public SynchroDbContext(DbContextOptions<SynchroDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("SYNCHRO");

            var thisAssembly = Assembly.GetExecutingAssembly();
            builder.ApplyConfigurationsFromAssembly(thisAssembly);
        }
    }
}
