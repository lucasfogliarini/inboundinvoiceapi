﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Infrastructure.Data
{
    public class FakeUnitOfWork : IUnitOfWork
    {
        List<IEntity> _collectionTracker = new List<IEntity>();
        public void Commit()
        {
            return;
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _collectionTracker.Add(entity);
        }
        public void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity
        {
            _collectionTracker.AddRange(entities);
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return _collectionTracker.OfType<TEntity>().AsQueryable();
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (entity.GetType() == typeof(InboundInvoiceIssueQueue))
            {
                var record = _collectionTracker.FirstOrDefault(x =>
                    x.GetType() == typeof(InboundInvoiceIssueQueue) &&
                    ((InboundInvoiceIssueQueue)x).Id == (entity as InboundInvoiceIssueQueue).Id);

                if (record is null)
                    throw new KeyNotFoundException();

                _collectionTracker.Remove(record);
            }

            _collectionTracker.Add(entity);
        }

        public void Dispose()
        {
            _collectionTracker = null;
        }

        public int ExecuteSqlCommand(string rawSqlString, params object[] parameters)
        {
            return 1;
        }

        public long GenerateSequence(string seqName)
        {
            return new Random().Next();
        }
    }
}
