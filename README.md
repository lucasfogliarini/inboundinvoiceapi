### Contributing
River Raid Team is responsible for letting this api working in all environments, any application health issues, please contact them.
For any merge-request, please add at least one member of River Raid to review and approve.

### Design
- API Framework _using_ [AspNetCore](https://www.nuget.org/packages/Microsoft.AspNetCore.App/)
- Dependency Injection Pattern _using_ [Microsoft.Extensions.DependencyInjection](https://www.nuget.org/packages/Microsoft.Extensions.DependencyInjection.Abstractions/)
- ORM Pattern with Fluent _using_ [EntityFrameworkCore](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore/) and [Oracle.EntityFrameworkCore](https://www.nuget.org/packages/Oracle.EntityFrameworkCore/)(__beta__)
- [UnitOfWork Pattern](https://martinfowler.com/eaaCatalog/unitOfWork.html) without Repository Pattern
- Tests _using_ [xunit](https://www.nuget.org/packages/xunit/)
- Authentication _using_ [Microsoft.AspNetCore.Authentication.JwtBearer](https://www.nuget.org/packages/Microsoft.AspNetCore.Authentication.JwtBearer/)
- Validation Pattern with Fluent _using_ [FluentValidation](https://www.nuget.org/packages/FluentValidation/)
- Logging _using_ [Microsoft.Extensions.Logging](https://www.nuget.org/packages/Microsoft.Extensions.Logging/) and [Seq.Extensions.Logging](https://www.nuget.org/packages/Seq.Extensions.Logging)
- PCF integration for configuration _using_ [Steeltoe.Extensions.Configuration.CloudFoundryCore](https://www.nuget.org/packages/Steeltoe.Extensions.Configuration.CloudFoundryCore/)
- API tool and documentation _using_ [Swashbuckle.AspNetCore](https://www.nuget.org/packages/Swashbuckle.AspNetCore/)

### Configuration
The __application configuration__ can be analyzed on __AddAppSettings__ method in [Startup.cs](https://gitlab.dell.com/finance/latam-finance-it/applications/inboundinvoiceapi/blob/master/Dell.Finance.BR.InboundInvoice.Api/Startup.cs#L59)

To use a [PCF App](https://docs.pivotal.io/pivotalcf/2-6/installing/index.html) must bind a [UserProvided Service](https://docs.cloudfoundry.org/devguide/services/user-provided.html)
with the same parameters from __AppSettings__ on [appsettings.Development.json](https://gitlab.dell.com/finance/latam-finance-it/applications/inboundinvoiceapi/blob/master/Dell.Finance.BR.InboundInvoice.Api/appsettings.Development.json)

### Environments and Deployment
The __environments__ and __deployment configuration__ can be analyzed on 
[gitlab-ci.yml](https://gitlab.dell.com/finance/latam-finance-it/applications/inboundinvoiceapi/blob/master/.gitlab-ci.yml)
and [manifest.yml](https://gitlab.dell.com/finance/latam-finance-it/applications/inboundinvoiceapi/blob/master/Dell.Finance.BR.InboundInvoice.Api/manifest.yml)

- [dev1](https://inboundinvoiceapi-dev1.ausvdc02.pcf.dell.com/swagger),
  [dev2](https://inboundinvoiceapi-dev2.ausvdc02.pcf.dell.com/swagger)
- [dit1](https://inboundinvoiceapi-dit1.ausvdc02.pcf.dell.com/swagger),
  [dit2](https://inboundinvoiceapi-dit2.ausvdc02.pcf.dell.com/swagger)
- [ge1](https://inboundinvoiceapi-ge1.ausvdc02.pcf.dell.com/swagger),
  [ge2](https://inboundinvoiceapi-ge2.ausvdc02.pcf.dell.com/swagger),
  [ge3](https://inboundinvoiceapi-ge3.ausvdc02.pcf.dell.com/swagger),
  [ge4](https://inboundinvoiceapi-ge4.ausvdc02.pcf.dell.com/swagger)
- [perf](https://inboundinvoiceapi-perf.ausvdc02.pcf.dell.com/swagger)

### Endpoints
The endpoints can be analyzed in the Swagger page of each application  

e.g.  
https://inboundinvoiceapi-ge2.ausvdc02.pcf.dell.com/swagger

### SonarQube

[Issues](http://ausdwsynapp02.aus.amer.dell.com:9000/project/issues?id=InboundInvoiceApi&resolved=false)  
[Coverage](http://ausdwsynapp02.aus.amer.dell.com:9000/component_measures?id=InboundInvoiceApi&metric=coverage)

### Fortify

[Fortify Pipeline Schedule](https://gitlab.dell.com/finance/latam-finance-it/applications/inboundinvoiceapi/pipeline_schedules)  
[Fortify App](https://fortify.dell.com/ssc/html/ssc/version/19054/overview/null/?filterSet=a243b195-0a59-3f8b-1403-d55b7a7d78e6)

### Repository (Gitlab)
https://gitlab.dell.com/finance/latam-finance-it/applications/inboundinvoiceapi

