﻿using Dell.Finance.BR.InboundInvoice.Core.Services;
using Dell.Finance.BR.InboundInvoice.Core.Services.Contracts;
using Dell.Finance.BR.InboundInvoice.Validators;
using Microsoft.Extensions.DependencyInjection;

namespace Dell.Finance.BR.InboundInvoice.Services
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IInboundInvoiceEMCService, InboundInvoiceEMCService>();
            services.AddTransient<IInboundInvoiceIFGService, InboundInvoiceIFGService>();
            services.AddTransient<IInboundInvoiceSNPService, InboundInvoiceSNPService>();
            services.AddTransient<IDollarRateService, DollarRateService>();
            services.AddTransient<IInvoiceItemDataService, InvoiceItemDataService>();
            services.AddTransient<ISynListValuesService, SynListValuesService>();
            services.AddTransient<IInboundInvoiceIssueQueueService, InboundInvoiceIssueQueueService>();
            return services;
        }
        public static IServiceCollection AddValidator(this IServiceCollection services)
        {
            services.AddSingleton<IValidator, Validator>();
            return services;
        }
    }
}