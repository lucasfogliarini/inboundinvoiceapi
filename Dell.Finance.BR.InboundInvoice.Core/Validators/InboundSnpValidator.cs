﻿using Dell.Finance.BR.InboundInvoice.Services;
using FluentValidation;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public class InboundSnpValidator : InboundValidator
    {
        public InboundSnpValidator()
        {
            RuleFor(i => i.RemetentePfjCodigo).Equal(InboundInvoiceSNPService.REMETENTE);
            RuleFor(i => i.Serie).Equal(InboundInvoiceSNPService.SERIE);
            RuleFor(i => i.NopCodigo).Equal(InboundInvoiceSNPService.NOP);
        }
    }
}
