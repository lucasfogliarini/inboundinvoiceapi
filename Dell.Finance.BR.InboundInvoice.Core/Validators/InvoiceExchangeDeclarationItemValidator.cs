﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using FluentValidation;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public class InvoiceExchangeDeclarationItemValidator : AbstractValidator<InvoiceExchangeDeclarationItem>
    {
        public InvoiceExchangeDeclarationItemValidator()
        {
            RuleFor(i => i.CodigoDoSite).Equal(ImportedInboundInvoiceService.CODIGO_DO_SITE);
            //RuleFor(i => i.CieId).NotEmpty(); can't test
            RuleFor(i => i.DofSequence).NotEmpty();
            RuleFor(i => i.IdfNum).NotEmpty();
            RuleFor(i => i.NumAdicao).NotEmpty();
            RuleFor(i => i.NumSeqAdicao).NotEmpty();
            RuleFor(i => i.NumDeclImpExp).Equal(ImportedInboundInvoiceService.DECLARATION_NUMERO);
            RuleFor(i => i.NumRegExport).Equal(ImportedInboundInvoiceService.DECLARATION_NUMERO_REGISTRO);
        }
    }
}
