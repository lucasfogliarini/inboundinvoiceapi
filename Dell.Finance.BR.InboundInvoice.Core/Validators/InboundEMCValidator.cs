﻿using Dell.Finance.BR.InboundInvoice.Services;
using FluentValidation;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public class InboundEMCValidator : InboundValidator
    {
        public InboundEMCValidator()
        {
            RuleFor(i => i.RemetentePfjCodigo).Equal(InboundInvoiceEMCService.REMETENTE);         
            RuleFor(i => i.Serie).Equal(InboundInvoiceEMCService.SERIE);
            RuleFor(i => i.NopCodigo).Equal(InboundInvoiceEMCService.NOP);
        }
    }
}
