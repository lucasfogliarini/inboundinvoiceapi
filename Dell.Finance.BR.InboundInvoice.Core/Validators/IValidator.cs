﻿using FluentValidation.Results;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public interface IValidator
    {
        ValidationResult Validate<TValidator>(object entity, bool throwIfNotValid = false) where TValidator : FluentValidation.IValidator;
    }
}
