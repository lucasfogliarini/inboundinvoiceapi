﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using FluentValidation;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public class InboundItemValidator : AbstractValidator<InvoiceItem>
    {
        public InboundItemValidator()
        {
            RuleFor(i => i.IdfNum).GreaterThan(0);
            RuleFor(i => i.CodigoDoSite).Equal(ImportedInboundInvoiceService.CODIGO_DO_SITE);
            RuleFor(i => i.DofSequence).GreaterThan(0);
            //RuleFor(i => i.DlbLaDomsOrder).NotEmpty();//emc dont use
            RuleFor(i => i.MercCodigo).NotEmpty();
            RuleFor(i => i.PrecoUnitario).GreaterThan(0);
            RuleFor(i => i.PrecoTotal).ScalePrecision(2, 9);
        }
    }
}
