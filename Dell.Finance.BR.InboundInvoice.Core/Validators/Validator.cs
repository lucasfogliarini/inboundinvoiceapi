﻿using FluentValidation.Results;
using Microsoft.Extensions.Logging;
using System;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public class Validator : IValidator
    {
        readonly ILogger<Validator> _logger;
        public Validator(ILogger<Validator> logger)
        {
            _logger = logger;
        }

        public ValidationResult Validate<TValidator>(object entity, bool throwIfNotValid = false) where TValidator : FluentValidation.IValidator
        {
            var validator = Activator.CreateInstance<TValidator>();
            var result = validator.Validate(entity);
            if (throwIfNotValid && !result.IsValid)
            {
                var message = result.ToString();
                _logger.LogError(message);
                throw new ApplicationException(message);
            }
            return result;
        }
    }
}
