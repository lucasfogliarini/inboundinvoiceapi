﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using FluentValidation;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public abstract class InboundValidator : AbstractValidator<Invoice>
    {
        public InboundValidator()
        {
            RuleFor(i => i.Id).Equal(i=>ImportedInboundInvoiceService.GetInvoiceId(i));
            RuleFor(i => i.ModoEmissao).Equal(ImportedInboundInvoiceService.MODO_EMISSAO);
            RuleFor(i => i.IndEntradaSaida).Equal(ImportedInboundInvoiceService.ENTRADA);
            RuleFor(i => i.CodigoDoSite).Equal(ImportedInboundInvoiceService.CODIGO_DO_SITE);
            RuleFor(i => i.SisCodigo).Equal(ImportedInboundInvoiceService.SIS_CODIGO);
            RuleFor(i => i.IndEix).Equal(ImportedInboundInvoiceService.IND_EIX);
            RuleFor(i => i.DofSequence).GreaterThan(0);
            RuleFor(i => i.EmitentePfjCodigo).NotEmpty();
            RuleFor(i => i.InformanteEstCodigo).NotEmpty();
            RuleFor(i => i.DestinatarioPfjCodigo).NotEmpty();
            RuleFor(i => i.DlbNumeroOrdemCompra).NotEmpty();
            RuleFor(i => i.DlbRecofNumOrder).NotEmpty();
            RuleFor(i => i.EdofCode).Equal(ImportedInboundInvoiceService.EDOF);
            RuleFor(i => i.MdofCode).Equal(ImportedInboundInvoiceService.MDOF);
            RuleFor(i => i.CfopCodigo).Equal(ImportedInboundInvoiceService.CFOP);
            RuleFor(i => i.PesoLiquido).Equal(ImportedInboundInvoiceService.PESO_LIQUIDO);
            RuleFor(i => i.PesoBruto).Equal(ImportedInboundInvoiceService.PESO_BRUTO);
            RuleFor(i => i.IndRespFrete).Equal(ImportedInboundInvoiceService.RESPONSAVEL_FRETE);
        }
    }
}
