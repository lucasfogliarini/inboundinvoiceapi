﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using FluentValidation;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public class OutboundValidator : AbstractValidator<Invoice>
    {
        public OutboundValidator()
        {
            When(x => x != null, () => 
            {
                RuleFor(x => x.IndEntradaSaida).Equal("S");
                RuleFor(x => x.Items).NotNull();
                RuleFor(x => x.EmitentePfjCodigo).NotEmpty();
                RuleFor(x => x.DestinatarioPfjCodigo).NotEmpty();
                RuleFor(x => x.DlbRecofNumOrder).NotEmpty();
            });
        }
    }
}
