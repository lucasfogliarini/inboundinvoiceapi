﻿using Dell.Finance.BR.InboundInvoice.Services;
using FluentValidation;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public class InboundIFGValidator : InboundValidator
    {
        public InboundIFGValidator()
        {      
            RuleFor(i => i.Serie).Equal(InboundInvoiceIFGService.SERIE);
            RuleFor(i => i.NopCodigo).Equal(InboundInvoiceIFGService.NOP);
        }
    }
}
