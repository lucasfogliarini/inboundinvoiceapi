﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using FluentValidation;
using System;

namespace Dell.Finance.BR.InboundInvoice.Validators
{
    public class InvoiceExchangeDeclarationValidator : AbstractValidator<InvoiceExchangeDeclaration>
    {
        public InvoiceExchangeDeclarationValidator()
        {
            var issuanceDate = DateTime.Now;
            RuleFor(i => i.CodigoDoSite).Equal(ImportedInboundInvoiceService.CODIGO_DO_SITE);
            RuleFor(i => i.DofSequence).NotEmpty();
            RuleFor(i => i.ExportadorPfjCodigo).NotEmpty();
            RuleFor(i => i.DesembaracoData).LessThanOrEqualTo(issuanceDate);
            RuleFor(i => i.DtDeclaracao).LessThanOrEqualTo(issuanceDate);
            RuleFor(i => i.DtEmiReDi).LessThanOrEqualTo(issuanceDate);
            RuleFor(i => i.DtRegExportacao).LessThanOrEqualTo(issuanceDate);
            RuleFor(i => i.NumDeclImpExp).Equal(ImportedInboundInvoiceService.DECLARATION_NUMERO);
            RuleFor(i => i.NumRegExport).Equal(ImportedInboundInvoiceService.DECLARATION_NUMERO_REGISTRO);            
            RuleFor(i => i.DesembaracoLoc).Equal(ImportedInboundInvoiceService.DECLARATION_DESEMBARACO_LOC);
            RuleFor(i => i.DesembaracoUf).Equal(ImportedInboundInvoiceService.DECLARATION_DESEMBARACO_UF);
            RuleFor(i => i.FormaImportacao).Equal(ImportedInboundInvoiceService.DECLARATION_FORMA_IMPORTACAO);
            RuleFor(i => i.NfeLocalizador).Equal(ImportedInboundInvoiceService.DECLARATION_NFE_LOCALIZADOR);
            RuleFor(i => i.ViaTransporte).Equal(ImportedInboundInvoiceService.DECLARATION_VIA_TRANSPORTE);
            RuleFor(i => i.VlAfrmm).Equal(ImportedInboundInvoiceService.DECLARATION_VL_AFRMM);
        }
    }
}
