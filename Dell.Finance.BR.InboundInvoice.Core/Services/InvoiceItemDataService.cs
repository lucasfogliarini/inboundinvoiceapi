﻿using Dell.Finance.BR.InboundInvoice.Core.Services.Contracts;
using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Core.Services
{
    internal sealed class InvoiceItemDataService : IInvoiceItemDataService
    {
        protected readonly IUnitOfWork unitOfWork;
        private readonly ILogger logger;

        public InvoiceItemDataService(IUnitOfWork unitOfWork,
                                      ILogger<InvoiceItemDataService> logger)
        {
            this.unitOfWork = unitOfWork;
            this.logger = logger;
        }

        public string GetIpOwner(long siteCode, long outboudnDofSequence, long idfNum)
        {
            try
            {
                return this.unitOfWork.Query<InvoiceItemData>()
                                .FirstOrDefault(x => x.CodigoDoSite == siteCode &&
                                                x.DofSequence == outboudnDofSequence &&
                                                x.IdfNum == idfNum)
                                .IpOwnerAttribute;
            }
            catch (NullReferenceException ex)
            {
                var message = $"Can not be found the Outbound DofSequence: {outboudnDofSequence}.";
                this.logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
            
        }
    }
}
