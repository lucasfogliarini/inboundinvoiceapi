﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Infrastructure.Entities;
using Dell.Finance.BR.InboundInvoice.Validators;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Services
{
    public class InboundInvoiceEMCService : ImportedInboundInvoiceService, IInboundInvoiceEMCService
    {
        public const string INBOUND_MARKUP_PRICE_SOFT_EMC = "INBOUND_MARKUP_PRICE_SOFT_EMC";

        public InboundInvoiceEMCService(IUnitOfWork unitOfWork,
            Validators.IValidator validator,
            ILogger<InboundInvoiceEMCService> logger)
            : base(unitOfWork, logger, validator)
        {
        }

        public long CreateInboundImportedSoftware(long outboundId)
        {
            var outbound = GetOutboundWithItems(outboundId);
            var relatedInboundId = GetRelatedInboundId(outbound.DlbRecofNumOrder);
            if (relatedInboundId.HasValue)
            {
                return relatedInboundId.Value;
            }

            _validator.Validate<OutboundValidator>(outbound, true);

            var newInbound = new Invoice();
            SetInboundInformation(newInbound, outbound);
            CloneItemsAndCalculateItemPrice(newInbound, outbound);
            SetInvoiceExchangeDeclaration(newInbound);

            _unitOfWork.Commit();

            var inboundId = GetInvoiceId(newInbound);
            CalculateInvoice(inboundId);

            return inboundId;
        }

        public void CloneItemsAndCalculateItemPrice(Invoice newInbound, Invoice outbound)
        {
            newInbound.Items = new List<InvoiceItem>();
            
            decimal itemPriceCostRate = GetItemPriceCostRate();
            decimal inboundCostRate = GetInboundCostRate();
            foreach (var outboundItem in outbound.Items)
            {
                decimal? ItemPrice = GetItemPrice(outboundItem.MercCodigo);
                var inboundPrice = CalculateInboundItemPrice(
                    outboundItem.PrecoUnitario,
                    inboundCostRate,
                    ItemPrice ?? 0,
                    itemPriceCostRate);
                var inboundItem = new InvoiceItem()
                {
                    Id = (GetInvoiceId(newInbound) * 1000000) + outboundItem.IdfNum,
                    IdfNum = outboundItem.IdfNum,
                    CodigoDoSite = newInbound.CodigoDoSite,
                    DofSequence = newInbound.DofSequence,
                    PrecoUnitario = inboundPrice,
                    PrecoTotal = Math.Round((inboundPrice * outboundItem.Qtd), 2),
                    MercCodigo = outboundItem.MercCodigo,
                    Qtd = outboundItem.Qtd,
                };
                _validator.Validate<InboundItemValidator>(inboundItem, true);
                newInbound.Items.Add(inboundItem);
            }

        }

        public decimal GetItemPriceCostRate()
        {
            try
            {
                var itemCostRate = _unitOfWork.Query<SynParameter>()
                    .FirstOrDefault(e => e.ParCodigo == INBOUND_MARKUP_PRICE_SOFT_EMC);
                decimal markupCostRate = Convert.ToDecimal(itemCostRate.Valor);

                return markupCostRate;
            }
            catch (Exception ex)
            {
                var message = $"Inbound Markup cost rate was not found. SYN_PARAMETROS.PAR_CODIGO: '{INBOUND_MARKUP_PRICE_SOFT_EMC}'";
                _logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
        }

        private decimal? GetItemPrice(string mercCodigo)
        {
            try
            {
                return _unitOfWork.Query<ListPrice>().FirstOrDefault(e => e.ItemCode == mercCodigo)?.Price;
            }
            catch (Exception ex)
            {
                var message = $"Item Price was not found ITEM_CODE: '{mercCodigo}'. ";
                _logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
        }

        public decimal CalculateInboundItemPrice(
            decimal outboundItemPrice,
            decimal inboundCostRate,
            decimal itemPrice,
            decimal itemPriceCostRate)
        {
            return itemPrice > 0 ? itemPrice * itemPriceCostRate / 100 : outboundItemPrice * inboundCostRate / 100;            
        }

        public void SetInboundInformation(Invoice newInbound, Invoice outbound)
        {
            newInbound.DofSequence = _unitOfWork.GenerateSequence(COR_SEQ_DOF);
            newInbound.DofImportNumero = GetDofImportNumber(outbound.DlbRecofNumOrder);
            newInbound.ModoEmissao = MODO_EMISSAO;
            newInbound.IndEntradaSaida = ENTRADA;
            newInbound.SisCodigo = SIS_CODIGO;
            newInbound.CodigoDoSite = CODIGO_DO_SITE;
            newInbound.EmitentePfjCodigo = outbound.RemetentePfjCodigo;
            newInbound.RemetentePfjCodigo = REMETENTE;
            newInbound.InformanteEstCodigo = outbound.EmitentePfjCodigo;
            newInbound.DestinatarioPfjCodigo = outbound.RemetentePfjCodigo;
            newInbound.EdofCode = EDOF;
            newInbound.MdofCode = MDOF;
            newInbound.Serie = SERIE;
            newInbound.CfopCodigo = CFOP;
            newInbound.NopCodigo = NOP;
            newInbound.DlbNumeroOrdemCompra = outbound.DlbRecofNumOrder;
            newInbound.DlbRecofNumOrder = outbound.DlbRecofNumOrder;
            newInbound.DellUsOrder = outbound.DellUsOrder;
            newInbound.PesoLiquido = PESO_LIQUIDO;
            newInbound.PesoBruto = PESO_BRUTO;
            newInbound.IndRespFrete = RESPONSAVEL_FRETE;
            newInbound.IndEix = IND_EIX;

            _unitOfWork.Add(newInbound);

            newInbound.Id = GetInvoiceId(newInbound);

            _validator.Validate<InboundEMCValidator>(newInbound, true);
        }

        public Invoice GetOutboundWithItems(long outboundId)
        {
            var query = _unitOfWork.Query<Invoice>()
                                   .Include(e => e.Items)
                                   .Where(e => e.Id == outboundId);
            return GetInvoice(query, outboundId);
        }

        private decimal GetInboundCostRate()
        {
            try
            {
                var inboundRateParameter = _unitOfWork.Query<SynParameter>().Where(e => e.ParCodigo == INBOUND_COST_RATE_SOFT_EMC).ToList().FirstOrDefault();
                decimal inboundCostRate = Convert.ToDecimal(inboundRateParameter.Valor);
                return inboundCostRate;
            }
            catch (Exception ex)
            {
                var message = $"Inbound cost rate was not found. SYN_PARAMETROS.PAR_CODIGO: '{INBOUND_COST_RATE_SOFT_EMC}'";
                _logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
        }

        public const string INBOUND_COST_RATE_SOFT_EMC = "INBOUND_COST_RATE_SOFT_EMC";

        public const string REMETENTE = "EMCF25";
        public const string SERIE = "1";
        public const string NOP = "E02.67";
    }
}