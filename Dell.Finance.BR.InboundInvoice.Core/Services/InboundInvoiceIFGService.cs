﻿using Dell.Finance.BR.InboundInvoice.Core.Services.Contracts;
using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Validators;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Services
{
    public class InboundInvoiceIFGService : ImportedInboundInvoiceService, IInboundInvoiceIFGService
    {
        private readonly IDollarRateService _dollarRateService;
        private readonly IInvoiceItemDataService _invoiceItemDataService;
        private readonly ISynListValuesService _synListValuesService;

        public InboundInvoiceIFGService(
            IUnitOfWork unitOfWork,
            IDollarRateService dollarRateService,
            Validators.IValidator validator,
            ILogger<InboundInvoiceIFGService> logger,
            IInvoiceItemDataService invoiceItemDataService,
            ISynListValuesService synListValuesService)
            : base(
                  unitOfWork,
                  logger,
                  validator)
        {
            _dollarRateService = dollarRateService;
            _invoiceItemDataService = invoiceItemDataService;
            _synListValuesService = synListValuesService;
        }

        public long CreateInboundImportedSoftware(long outboundId)
        {
            var outbound = GetOutboundWithItems(outboundId);
            var relatedInboundId = GetRelatedInboundId(outbound.DlbRecofNumOrder);
            if (relatedInboundId.HasValue)
            {
                return relatedInboundId.Value;
            }

            _validator.Validate<OutboundValidator>(outbound, true);

            var newInbound = new Invoice();
            var ipOwner = _invoiceItemDataService.GetIpOwner(
                outbound.CodigoDoSite,
                outbound.DofSequence,
                outbound.Items.First().IdfNum);
            var pfjCode = _synListValuesService.GetSenderPfjCode(ipOwner, outbound);

            SetInboundInformation(newInbound, outbound, pfjCode);
            CloneItemAndCalculateItemPrice(newInbound, outbound);
            SetInvoiceExchangeDeclaration(newInbound);

            _unitOfWork.Commit();

            var inboundId = GetInvoiceId(newInbound);
            CalculateInvoice(inboundId);

            return inboundId;
        }

        private void SetInboundInformation(
            Invoice newInbound,
            Invoice outbound,
            string pfjCode)
        {
            newInbound.DofSequence = _unitOfWork.GenerateSequence(COR_SEQ_DOF);
            newInbound.ModoEmissao = MODO_EMISSAO;
            newInbound.IndEntradaSaida = ENTRADA;
            newInbound.SisCodigo = SIS_CODIGO;
            newInbound.DofImportNumero = GetDofImportNumber(outbound.DlbRecofNumOrder);
            newInbound.CodigoDoSite = CODIGO_DO_SITE;
            newInbound.EmitentePfjCodigo = outbound.RemetentePfjCodigo;
            newInbound.RemetentePfjCodigo = pfjCode;
            newInbound.InformanteEstCodigo = outbound.EmitentePfjCodigo;
            newInbound.DestinatarioPfjCodigo = outbound.RemetentePfjCodigo;
            newInbound.EdofCode = EDOF;
            newInbound.MdofCode = MDOF;
            newInbound.Serie = SERIE;
            newInbound.CfopCodigo = CFOP;
            newInbound.NopCodigo = NOP;
            newInbound.DlbNumeroOrdemCompra = outbound.DlbRecofNumOrder;
            newInbound.DlbRecofNumOrder = outbound.DlbRecofNumOrder;
            newInbound.DellUsOrder = outbound.DellUsOrder;
            newInbound.PesoLiquido = PESO_LIQUIDO;
            newInbound.PesoBruto = PESO_BRUTO;
            newInbound.IndRespFrete = RESPONSAVEL_FRETE;
            newInbound.IndEix = IND_EIX;

            _unitOfWork.Add(newInbound);

            newInbound.Id = GetInvoiceId(newInbound);

            _validator.Validate<InboundIFGValidator>(newInbound, true);
        }

        public Invoice GetOutboundWithItems(long outboundId)
        {
            var query = _unitOfWork.Query<Invoice>()
                        .Include(e => e.Items)
                        .ThenInclude(e => e.InvoiceItemData)
                        .Where(e => e.Id == outboundId);

            return GetInvoice(query, outboundId);
        }

        public void CloneItemAndCalculateItemPrice(Invoice newInbound, Invoice outbound)
        {
            newInbound.Items = new List<InvoiceItem>();
            int idfNumber = 1;
            var inboundItemPrice = CalculateInboundItemPrice(outbound.Items);
            var outboundItem = outbound.Items.SingleOrDefault(i=> i.Qtd > 0);
            var inboundItem = new InvoiceItem()
            {
                Id = (GetInvoiceId(newInbound) * 1000000) + idfNumber,
                IdfNum = idfNumber,
                CodigoDoSite = newInbound.CodigoDoSite,
                DofSequence = newInbound.DofSequence,
                PrecoUnitario = inboundItemPrice,
                PrecoTotal = Math.Round((inboundItemPrice * outboundItem.Qtd),2),
                MercCodigo = outboundItem.MercCodigo,
                Qtd = outboundItem.Qtd,
                DlbLaDomsOrder = outbound.DlbRecofNumOrder,
            };
            _validator.Validate<InboundItemValidator>(inboundItem, true);
            newInbound.Items.Add(inboundItem);
        }

        public decimal CalculateInboundItemPrice(IEnumerable<InvoiceItem> outboundItems)
        {
            try
            {
                var filteredItems = outboundItems.Where(e => e.InvoiceItemData.FulfillmentLocId == "00");
                var sumPrice = filteredItems.Sum(e => e.InvoiceItemData.PrecoUnitarioUSD).GetValueOrDefault();
                var dollarRate = _dollarRateService.GetDollarRate();
                var inboundItemPrice = sumPrice * dollarRate;

                return inboundItemPrice;
            }
            catch (Exception ex)
            {
                var message = $"An error occurred while calculating the inbound invoice item price.";
                _logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
        }

        public const string SERIE = "5";
        public const string NOP = "E02.15";
    }
}