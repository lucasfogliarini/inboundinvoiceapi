﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Validators;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Services
{
    public abstract class ImportedInboundInvoiceService
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly ILogger _logger;
        protected readonly IValidator _validator;

        public ImportedInboundInvoiceService(IUnitOfWork unitOfWork, ILogger logger, IValidator validator)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _validator = validator;
        }
        public static long GetInvoiceId(Invoice invoice)
        {
            return (invoice.DofSequence * 1000) + 1;
        }


        public long? GetRelatedInboundId(string recofNumOrder)
        {
            var relatedInbound = _unitOfWork.Query<Invoice>()
                .Where(e => e.DlbRecofNumOrder == recofNumOrder && e.IndEntradaSaida == ENTRADA).ToList();
            if (relatedInbound.Count == 1)
            {
                return relatedInbound.Single().Id;
            }
            if (relatedInbound.Count > 1)
            {
                var errorMessage = $"There is more than one inbound invoice with the same order number. DLB_RECOF_NUM_ORDER: {recofNumOrder}";
                _logger.LogError(errorMessage);
                throw new ApplicationException(errorMessage);
            }

            return null;
        }

        protected string GetDofImportNumber(string numeroOrdem)
        {
            return $"{SIS_CODIGO}-{numeroOrdem}";
        }

        protected void CalculateInvoice(long invoiceId)
        {
            string calculateProcedureName = "EXFCALC_EXECUTA";
            try
            {
                _unitOfWork.ExecuteSqlCommand($"BEGIN {calculateProcedureName}({invoiceId}); END;");
            }
            catch (Exception ex)
            {
                var message = $"An error occurred while calculating the invoice. {calculateProcedureName}";
                _logger.LogError(message, ex);
            }
        }

        protected Invoice GetInvoice(IQueryable<Invoice> query, long invoiceId)
        {
            var invoice = query.ToList().FirstOrDefault();
            if (invoice == null)
            {
                var message = $"There is no invoice with id: {invoiceId}";
                _logger.LogWarning(message);
                throw new ApplicationException(message);
            }

            return invoice;
        }

        public void SetInvoiceExchangeDeclaration(Invoice newInbound)
        {
            var issuanceDate = DateTime.Now;
            var declaration = new InvoiceExchangeDeclaration()
            {
                Id = _unitOfWork.GenerateSequence(COR_IMP_EXP_SEQ),
                CodigoDoSite = newInbound.CodigoDoSite,
                DofSequence = newInbound.DofSequence,
                NumRegExport = DECLARATION_NUMERO_REGISTRO,
                NumDeclImpExp = DECLARATION_NUMERO,
                //Número do Conhecimento de Transporte ?
                NfeLocalizador = DECLARATION_NFE_LOCALIZADOR,
                DtEmiReDi = issuanceDate,
                DtRegExportacao = issuanceDate,
                DtDeclaracao = issuanceDate,
                FormaImportacao = DECLARATION_FORMA_IMPORTACAO,
                //Data da Averbação ?
                ExportadorPfjCodigo = newInbound.RemetentePfjCodigo,
                ViaTransporte = DECLARATION_VIA_TRANSPORTE,
                VlAfrmm = DECLARATION_VL_AFRMM,
                DesembaracoLoc = DECLARATION_DESEMBARACO_LOC,
                DesembaracoUf = DECLARATION_DESEMBARACO_UF,
                DesembaracoData = issuanceDate,
            };

            _validator.Validate<InvoiceExchangeDeclarationValidator>(declaration, true);
            _unitOfWork.Add(declaration);

            SetInvoiceExchangeDeclarationItem(newInbound, declaration);
        }

        private void SetInvoiceExchangeDeclarationItem(Invoice newInbound, InvoiceExchangeDeclaration declaration)
        {
            foreach (var item in newInbound.Items)
            {
                var declarationItem = new InvoiceExchangeDeclarationItem()
                {
                    CodigoDoSite = declaration.CodigoDoSite,
                    DofSequence = declaration.DofSequence,
                    NumAdicao = 999,
                    NumSeqAdicao = 1,
                    FabricantePfjCodigo = "",
                    DescontoAdicao = 0,
                    NumDrawback = "",
                    IdfNum = item.IdfNum,
                    NumDeclImpExp = declaration.NumDeclImpExp,
                    CieId = declaration.Id,
                    NumRegExport = declaration.NumRegExport,
                    NumRegExportNew = "",
                };

                _validator.Validate<InvoiceExchangeDeclarationItemValidator>(declarationItem, true);

                _unitOfWork.Add(declarationItem);
            }
        }

        public const string COR_IMP_EXP_SEQ = "COR_IMP_EXP_SEQ";
        public const string COR_SEQ_DOF = "COR_SEQ_DOF";
        public const string ENTRADA = "E";
        public const string MODO_EMISSAO = "I";
        public const long CODIGO_DO_SITE = 1;
        public const string MDOF = "55";
        public const string EDOF = "NFE";
        public const string SIS_CODIGO = "16";
        public const string CFOP = "3.102";
        public const decimal PESO_LIQUIDO = 0;
        public const decimal PESO_BRUTO = 1;
        public const string RESPONSAVEL_FRETE = null;
        public const string IND_EIX = "X";

        public const string DECLARATION_NUMERO = "NIHIL";
        public const string DECLARATION_NUMERO_REGISTRO = "0";
        public const string DECLARATION_NFE_LOCALIZADOR = "0";
        public const string DECLARATION_FORMA_IMPORTACAO = "1";
        public const string DECLARATION_VIA_TRANSPORTE = "4";
        public const decimal DECLARATION_VL_AFRMM = 0m;
        public const string DECLARATION_DESEMBARACO_LOC = "São Paulo";
        public const string DECLARATION_DESEMBARACO_UF = "SP";
    }
}