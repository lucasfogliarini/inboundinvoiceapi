﻿using Dell.Finance.BR.InboundInvoice.Core.Services.Contracts;
using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Infrastructure.Data;
using Dell.Finance.Framework.Core.Mailer.Contracts;
using Dell.Finance.Framework.Core.Mailer.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Core.Services
{
    internal sealed class SynListValuesService : ISynListValuesService
    {
        protected readonly IUnitOfWork unitOfWork;
        protected readonly ILogger logger;
        protected readonly AppSettings appSettings;
        protected readonly IEmailService emailService;
        protected readonly IInboundInvoiceIssueQueueService inboundInvoiceIssueQueueService;
        protected readonly SmtpConfiguration smtpConfiguration;

        private const string IP_OWNER_EMAIL_LIST = "IP_OWNER_EMAIL_LIST";

        public SynListValuesService(
            IUnitOfWork unitOfWork,
            ILogger<SynListValuesService> logger,
            AppSettings appSettings,
            SmtpConfiguration smtpConfiguration,
            IEmailService emailService,
            IInboundInvoiceIssueQueueService inboundInvoiceIssueQueueService)
        {
            this.unitOfWork = unitOfWork;
            this.logger = logger;
            this.appSettings = appSettings;
            this.smtpConfiguration = smtpConfiguration;
            this.emailService = emailService;
            this.inboundInvoiceIssueQueueService = inboundInvoiceIssueQueueService;
        }

        string ISynListValuesService.GetSenderPfjCode(
            string ipOwner,
            Invoice outbound)
        {
            try
            {
                return this.unitOfWork.Query<SynListValues>()
                .FirstOrDefault(x => x.ListaTipo.Equals("IP_OWNER_LIST") && x.ListaValor.Equals(string.IsNullOrEmpty(ipOwner) ? "EMPTY" : ipOwner))
                .Description;
            }
            catch (NullReferenceException ex)
            {
                var message = $"Can not be found the Ip Owner Attribute: {ipOwner}.";
                this.logger.LogError(message, ex);

                SendEmailIpOwnerError(ipOwner, outbound);

                throw new ApplicationException(message, ex);
            }
        }

        private void SendEmailIpOwnerError(
            string ipOwner,
            Invoice outbound)
        {
            try
            {
                var inboundNFIssueQueue = this.inboundInvoiceIssueQueueService.GetByOutboundId(outbound.Id);

                if (!inboundNFIssueQueue.LastEmailSent.HasValue ||
                    inboundNFIssueQueue.LastEmailSent.Value.Date < DateTime.Now.Date)
                {
                    SendEmailProccess(ipOwner, outbound);

                    inboundNFIssueQueue.LastEmailSent = DateTime.Now;
                    this.inboundInvoiceIssueQueueService.UpdateInboundInvoiceIssueQueue(inboundNFIssueQueue);
                }
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
            }
        }

        private void SendEmailProccess(
            string ipOwner,
            Invoice outbound)
        {
            try
            {
                var emailMessage = BuildEmailMessage(
                    ipOwner,
                    outbound);

                emailService.Send(emailMessage);

                this.logger.LogInformation(
                    "EmailSent",
                    emailMessage.Content);
            }
            catch (Exception ex)
            {
                this.logger.LogError($"Error to send email {ex.Message}", ex);
            }
        }

        private EmailMessage BuildEmailMessage(
            string ipOwner,
            Invoice outbound)
        {
            var fromEmail = this.appSettings.FromEmail.Split(';').ToArray();
            var fromAddresses = fromEmail.Select(x => new EmailAddress { Address = x }).ToList();

            var toEmails = this.GetIPOwnerEmailList().Split(';').Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            var toAddresses = toEmails.Select(x => new EmailAddress { Address = x }).ToList();

            var subject = "[ACTION NEEDED] Error on the Dell Software IFG process";

            var content = new MimeKit.BodyBuilder();
            content.TextBody = $@"
Error on the Dell Software IFG process - The IP OWNER value is missing
                
Date: {DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss tt")}

Sale Order: {outbound.DofSequence} | Dof Import Number: {outbound.DofImportNumero}

Description: The IP Owner {ipOwner} is not configured in Synchro. The inbound NF will not be created and the sale order will not be issued. Please review the IP_OWNER_LIST.";

            var emailMessage = new EmailMessage(
                content,
                subject,
                fromAddresses,
                toAddresses);

            return emailMessage;
        }

        public string GetIPOwnerEmailList()
        {
            try
            {
                var ipOwnerEmailList = unitOfWork.Query<SynParameter>().First(e => e.ParCodigo == IP_OWNER_EMAIL_LIST);
                return ipOwnerEmailList.Valor;
            }
            catch (Exception ex)
            {
                var message = $"Ip owner email list was not found. SYN_PARAMETROS.PAR_CODIGO: '{IP_OWNER_EMAIL_LIST}'";
                logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
        }
    }
}
