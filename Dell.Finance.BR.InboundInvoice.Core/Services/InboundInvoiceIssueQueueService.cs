﻿using Dell.Finance.BR.InboundInvoice.Core.Services.Contracts;
using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Core.Services
{
    internal sealed class InboundInvoiceIssueQueueService : IInboundInvoiceIssueQueueService
    {
        protected readonly IUnitOfWork unitOfWork;
        private readonly ILogger logger;

        public InboundInvoiceIssueQueueService(
            IUnitOfWork unitOfWork,
            ILogger<InvoiceItemDataService> logger)
        {
            this.unitOfWork = unitOfWork;
            this.logger = logger;
        }

        InboundInvoiceIssueQueue IInboundInvoiceIssueQueueService.GetByOutboundId(long outboundDofId)
        {
            try
            {
                return this.unitOfWork.Query<InboundInvoiceIssueQueue>().FirstOrDefault(x => x.OutboundDofId == outboundDofId);
            }
            catch (NullReferenceException ex)
            {
                var message = $"Can not be found by Outbound DofSequence: {outboundDofId}.";
                this.logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
        }

        void IInboundInvoiceIssueQueueService.UpdateInboundInvoiceIssueQueue(InboundInvoiceIssueQueue inboundInvoiceIssueQueue)
        {
            try
            {
                this.unitOfWork.Update<InboundInvoiceIssueQueue>(inboundInvoiceIssueQueue);
                this.unitOfWork.Commit();
            }
            catch (NullReferenceException ex)
            {
                var message = $"Can not update Inbound Invoice Issue Queue: {inboundInvoiceIssueQueue.Id}.";
                this.logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
        }
    }
}
