﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;

namespace Dell.Finance.BR.InboundInvoice.Services
{
    public interface IInboundInvoiceEMCService
    {
        long CreateInboundImportedSoftware(long outboundId);
        void SetInboundInformation(Invoice newInbound, Invoice outbound);
        void SetInvoiceExchangeDeclaration(Invoice newInbound);
        decimal CalculateInboundItemPrice(decimal outboundItemPrice, decimal inboundCostRate, decimal itemPrice, decimal itemPriceCostRate);
        void CloneItemsAndCalculateItemPrice(Invoice newInbound, Invoice outbound);
        Invoice GetOutboundWithItems(long outboundId);
        long? GetRelatedInboundId(string recofNumOrder);
        decimal GetItemPriceCostRate();
    }
}