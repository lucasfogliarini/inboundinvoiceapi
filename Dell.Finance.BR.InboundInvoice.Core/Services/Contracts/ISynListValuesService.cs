﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;

namespace Dell.Finance.BR.InboundInvoice.Core.Services.Contracts
{
    public interface ISynListValuesService
    {
        string GetSenderPfjCode(string ipOwner, Invoice outbound);
    }
}
