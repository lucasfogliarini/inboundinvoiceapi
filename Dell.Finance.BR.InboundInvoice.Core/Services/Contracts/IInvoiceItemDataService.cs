﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dell.Finance.BR.InboundInvoice.Core.Services.Contracts
{
    public interface IInvoiceItemDataService
    {
        string GetIpOwner(long siteCode, long outboudnDofSequence, long idfNum);
    }
}
