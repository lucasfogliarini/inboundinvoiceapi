﻿namespace Dell.Finance.BR.InboundInvoice.Services
{
    public interface IDollarRateService
    {
        decimal GetDollarRate();
    }
}