﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using System.Collections.Generic;

namespace Dell.Finance.BR.InboundInvoice.Services
{
    public interface IInboundInvoiceIFGService
    {
        long CreateInboundImportedSoftware(long outboundId);
        Invoice GetOutboundWithItems(long outboundId);
        void CloneItemAndCalculateItemPrice(Invoice newInbound, Invoice outbound);
        decimal CalculateInboundItemPrice(IEnumerable<InvoiceItem> outboundItems);
    }
}