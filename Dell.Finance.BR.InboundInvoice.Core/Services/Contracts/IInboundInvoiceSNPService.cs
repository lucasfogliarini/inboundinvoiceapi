﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;

namespace Dell.Finance.BR.InboundInvoice.Services
{
    public interface IInboundInvoiceSNPService
    {
        long CreateInboundImportedSoftware(long outboundId);
        Invoice GetOutboundWithItems(long outboundId);
        void CloneItemsAndCalculateItemUnitPrice(Invoice newInbound, Invoice outbound);
        decimal CalculateInboundItemUnitPrice(decimal outboundItemPriceUSD, decimal dollarRate, decimal markupPercentage);
        decimal GetMarkupPercentage(string pfjCodigo);
    }
}