﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dell.Finance.BR.InboundInvoice.Core.Services.Contracts
{
    public interface IInboundInvoiceIssueQueueService
    {
        InboundInvoiceIssueQueue GetByOutboundId(long outboundDofId);

        void UpdateInboundInvoiceIssueQueue(InboundInvoiceIssueQueue inboundInvoiceIssueQueue);
    }
}
