﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Validators;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Services
{
    public class InboundInvoiceSNPService : ImportedInboundInvoiceService, IInboundInvoiceSNPService
    {
        private readonly IDollarRateService _dollarRateService;
        public InboundInvoiceSNPService(IUnitOfWork unitOfWork, IDollarRateService dollarRateService, Validators.IValidator validator, ILogger<InboundInvoiceIFGService> logger)
            : base(unitOfWork, logger, validator)
        {
            _dollarRateService = dollarRateService;
        }

        public long CreateInboundImportedSoftware(long outboundId)
        {
            var outbound = GetOutboundWithItems(outboundId);
            var relatedInboundId = GetRelatedInboundId(outbound.DlbRecofNumOrder);
            if (relatedInboundId.HasValue)
            {
                return relatedInboundId.Value;
            }

            _validator.Validate<OutboundValidator>(outbound, true);

            var newInbound = new Invoice();
            SetInboundInformation(newInbound, outbound);
            CloneItemsAndCalculateItemUnitPrice(newInbound, outbound);
            SetInvoiceExchangeDeclaration(newInbound);

            _unitOfWork.Commit();

            var inboundId = GetInvoiceId(newInbound);
            CalculateInvoice(inboundId);

            return inboundId;
        }

        public void SetInboundInformation(Invoice newInbound, Invoice outbound)
        {
            newInbound.DofSequence = _unitOfWork.GenerateSequence(COR_SEQ_DOF);
            newInbound.ModoEmissao = MODO_EMISSAO;
            newInbound.IndEntradaSaida = ENTRADA;
            newInbound.SisCodigo = SIS_CODIGO;
            newInbound.DofImportNumero = GetDofImportNumber(outbound.DlbRecofNumOrder);
            newInbound.CodigoDoSite = CODIGO_DO_SITE;
            newInbound.EmitentePfjCodigo = outbound.RemetentePfjCodigo;
            newInbound.RemetentePfjCodigo = REMETENTE;
            newInbound.InformanteEstCodigo = outbound.EmitentePfjCodigo;
            newInbound.DestinatarioPfjCodigo = outbound.RemetentePfjCodigo;
            newInbound.EdofCode = EDOF;
            newInbound.MdofCode = MDOF;
            newInbound.Serie = SERIE;
            newInbound.CfopCodigo = CFOP;
            newInbound.NopCodigo = NOP;
            newInbound.DlbNumeroOrdemCompra = outbound.DlbRecofNumOrder;
            newInbound.DlbRecofNumOrder = outbound.DlbRecofNumOrder;
            newInbound.DellUsOrder = outbound.DellUsOrder;
            newInbound.PesoLiquido = PESO_LIQUIDO;
            newInbound.PesoBruto = PESO_BRUTO;
            newInbound.IndRespFrete = RESPONSAVEL_FRETE;
            newInbound.IndEix = IND_EIX;

            _unitOfWork.Add(newInbound);

            newInbound.Id = GetInvoiceId(newInbound);

            _validator.Validate<InboundSnpValidator>(newInbound, true);
        }

        public Invoice GetOutboundWithItems(long outboundId)
        {
            var query = _unitOfWork.Query<Invoice>()
                        .Include(e => e.Items)
                        .ThenInclude(e => e.InvoiceItemData)
                        .Where(e => e.Id == outboundId);

            return GetInvoice(query, outboundId);
        }

        public void CloneItemsAndCalculateItemUnitPrice(Invoice newInbound, Invoice outbound)
        {
            newInbound.Items = new List<InvoiceItem>();
            decimal markupPercentage = GetMarkupPercentage(REMETENTE);
            decimal dollarRate = _dollarRateService.GetDollarRate();
            var outboundItems = outbound.Items.Where(e => e.Qtd > 0);
            foreach (var outboundItem in outboundItems)
            {
                var outboundItemUnitPriceUSD = outboundItem.InvoiceItemData?.PrecoUnitarioUSD;
                var inboundItemUnitPrice = CalculateInboundItemUnitPrice(outboundItemUnitPriceUSD.GetValueOrDefault(), dollarRate, markupPercentage);
                var inboundItem = new InvoiceItem()
                {
                    Id = (GetInvoiceId(newInbound) * 1000000) + outboundItem.IdfNum,
                    IdfNum = outboundItem.IdfNum,
                    CodigoDoSite = newInbound.CodigoDoSite,
                    DofSequence = newInbound.DofSequence,
                    PrecoUnitario = inboundItemUnitPrice,
                    PrecoTotal = Math.Round((inboundItemUnitPrice * outboundItem.Qtd), 2),
                    MercCodigo = outboundItem.MercCodigo,
                    Qtd = outboundItem.Qtd,
                };
                _validator.Validate<InboundItemValidator>(inboundItem, true);
                newInbound.Items.Add(inboundItem);
            }
        }
        public decimal CalculateInboundItemUnitPrice(decimal outboundItemUnitPriceUSD, decimal dollarRate, decimal markupPercentage)
        {
            try
            {
                var inboundItemUnitPrice = (outboundItemUnitPriceUSD * dollarRate) * (1 - markupPercentage);

                return inboundItemUnitPrice;
            }
            catch (Exception ex)
            {
                var message = $"An error occurred while calculating the inbound invoice item price.";
                _logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
        }
        public decimal GetMarkupPercentage(string pfjCodigo)
        {
            try
            {
                var markupTypePfjCodigo = _unitOfWork.Query<SynListValues>().Where(e => e.ListaValor == pfjCodigo).Single();
                var markupType = markupTypePfjCodigo.ListaTipo == MICROSOFT_SNP_PFJ_CODIGOS ? MICROSOFT_SNP_MARKUP : VMWARE_SNP_MARKUP;

                var markupPercentage = _unitOfWork.Query<SynParameter>().Where(e => e.ParCodigo == markupType).FirstOrDefault();

                return Convert.ToDecimal(markupPercentage.Valor) / 100;
            }
            catch (Exception ex)
            {
                var message = $"Markup Percentage was not found. PFJ_CODIGO: {pfjCodigo}";
                _logger.LogError(message, ex);
                throw new ApplicationException(message, ex);
            }
        }

        public const string VMWARE_SNP_MARKUP = "VMWARE_SNP_MARKUP";
        public const string MICROSOFT_SNP_MARKUP = "MICROSOFT_SNP_MARKUP";
        public const string VMWARE_SNP_PFJ_CODIGOS = "VMWARE_SNP_PFJ_CODIGOS";
        public const string MICROSOFT_SNP_PFJ_CODIGOS = "MICROSOFT_SNP_PFJ_CODIGOS";
        public const string REMETENTE = "544346F";
        public const string SERIE = "5";
        public const string NOP = "E02.05";
    }
}