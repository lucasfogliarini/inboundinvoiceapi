﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace Dell.Finance.BR.InboundInvoice.Services
{
    public class DollarRateService : IDollarRateService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public DollarRateService(IUnitOfWork unitOfWork, ILogger<DollarRateService> logger)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public decimal GetDollarRate()
        {
            var period = DateTime.Now.Date.AddDays(-30);
            var dollarRate = _unitOfWork.Query<Invoice>()
                .Where(x => x.SiglaMoeda == SIGLA_MOEDA_USD
                         && x.SisCodigo == SIS_CODIGO_EDI
                         && x.DtFatoGeradorImposto >= period)
                .OrderByDescending(x => x.DtFatoGeradorImposto)
                .FirstOrDefault()?.TxConversaoMoedaNac ?? 0m;

            if (dollarRate == 0)
            {
                var message = $"DollarRate was not found. Condition: DT_FATO_GERADOR_IMPOSTO >= {period}";
                _logger.LogError(message);
                throw new ApplicationException(message);
            }

            return dollarRate;
        }
        public const string SIGLA_MOEDA_USD = "USD";
        public const string SIS_CODIGO_EDI = "EDI";
    }
}