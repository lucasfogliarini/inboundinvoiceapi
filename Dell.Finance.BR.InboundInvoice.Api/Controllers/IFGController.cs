﻿using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dell.Finance.BR.InboundInvoice.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IFGController : BaseController
    {
        private readonly IInboundInvoiceIFGService _inboundInvoiceIFGService;
        public IFGController(IInboundInvoiceIFGService inboundInvoiceIFGService, ILogger<IFGController> logger) : base(logger)
        {
            _inboundInvoiceIFGService = inboundInvoiceIFGService;
        }

        /// <summary>
        /// Create a IFG Inbound Invoice Imported Software passing a outbound invoice id.
        /// </summary>
        /// <remarks>
        /// [Process](https://api.genmymodel.com/projects/_xtATsOUVEemtQJ5uHCArog/diagrams/_596yIMdiEDe5tZKDpR_1NQ/svg)  
        /// [Subprocess](https://api.genmymodel.com/projects/_xtATsOUVEemtQJ5uHCArog/diagrams/_DMkTAMdjEDe5tZKDpR_1NQ/svg)
        /// 
        /// Check if outbound is a IFG: 
        /// ```
        /// select LATAM_FINANCE.IMPORTED_SOFTWARE_INVOICE_PKG.IS_IFG_PROCESS_TYPE({outboundId}) IS_IFG from dual;
        /// ```
        /// </remarks>
        /// <param name="outboundId">The outbound invoice id</param>
        /// <returns>The inbound invoice id created</returns>
        [HttpPost, Route("CreateImportedSoftware")]
        public IActionResult CreateImportedSoftware([FromBody] long outboundId)
        {
            return TryProcess(() =>
            {
                var inboundId = _inboundInvoiceIFGService.CreateInboundImportedSoftware(outboundId);
                return Ok(inboundId);
            });
        }
    }
}
