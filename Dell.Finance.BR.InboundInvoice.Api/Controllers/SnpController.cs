﻿using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dell.Finance.BR.InboundInvoice.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SnpController : BaseController
    {
        private readonly IInboundInvoiceSNPService _inboundInvoiceSnpService;
        public SnpController(IInboundInvoiceSNPService inboundInvoiceSnpService, ILogger<SnpController> logger) : base(logger)
        {
            _inboundInvoiceSnpService = inboundInvoiceSnpService;
        }

        /// <summary>
        /// Create a SNP Inbound Invoice Imported Software passing a outbound invoice id.
        /// </summary>
        /// <remarks>
        /// Check if outbound is a SNP: 
        /// ```
        /// select LATAM_FINANCE.IMPORTED_SOFTWARE_INVOICE_PKG.IS_SNP_PROCESS_TYPE({outboundId}) IS_SNP from dual;
        /// ```
        /// </remarks>
        /// <param name="outboundId">The outbound invoice id</param>
        /// <returns>The inbound invoice id created</returns>
        [HttpPost, Route("CreateImportedSoftware")]
        public IActionResult CreateImportedSoftware([FromBody] long outboundId)
        {
            return TryProcess(() =>
            {
                var inboundId = _inboundInvoiceSnpService.CreateInboundImportedSoftware(outboundId);
                return Ok(inboundId);
            });
        }
    }
}
