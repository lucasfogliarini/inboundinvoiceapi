﻿using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dell.Finance.BR.InboundInvoice.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EMCController : BaseController
    {
        private readonly IInboundInvoiceEMCService _inboundInvoiceEMCService;
        public EMCController(IInboundInvoiceEMCService inboundInvoiceEMCService, ILogger<EMCController> logger) : base(logger)
        {
            _inboundInvoiceEMCService = inboundInvoiceEMCService;
        }

        /// <summary>
        /// Create a EMC Inbound Invoice Imported Software passing a outbound invoice id.
        /// </summary>
        /// <remarks>
        /// [Process diagram](https://api.genmymodel.com/projects/_xtATsOUVEemtQJ5uHCArog/diagrams/_vR6X4MdhEDe5tZKDpR_1NQ/svg)  
        /// [Subprocess diagram](https://api.genmymodel.com/projects/_xtATsOUVEemtQJ5uHCArog/diagrams/_vtAYc8dhEDe5tZKDpR_1NQ/svg)  
        /// 
        /// Check if outbound is a EMC: 
        /// ```
        /// select LATAM_FINANCE.IMPORTED_SOFTWARE_INVOICE_PKG.IS_EMC_PROCESS_TYPE({outboundId}) IS_EMC from dual;
        /// ```
        /// </remarks>
        /// <param name="outboundId">The outbound invoice id</param>
        /// <returns>The inbound invoice id created</returns>
        [HttpPost, Route("CreateImportedSoftware")]
        public IActionResult CreateImportedSoftware([FromBody] long outboundId)
        {
            return TryProcess(() =>
            {
                var inboundId = _inboundInvoiceEMCService.CreateInboundImportedSoftware(outboundId);
                return Ok(inboundId);
            });
        }
    }
}
