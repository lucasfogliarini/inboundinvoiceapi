﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Dell.Finance.BR.InboundInvoice.Api.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        private readonly ILogger _logger;

        public BaseController(ILogger logger)
        {
            _logger = logger;
        }
        protected IActionResult TryProcess(Func<IActionResult> process)
        {
            try
            {
                return process();
            }
            catch (ApplicationException ex)
            {
                return this.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "An unexpected error has occurred.");
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }
    }
}
