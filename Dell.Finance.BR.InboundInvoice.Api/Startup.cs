﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Infrastructure.Data;
using Dell.Finance.BR.InboundInvoice.Services;
using Dell.Finance.Framework.Core.Mailer;
using Dell.Finance.Framework.Core.Mailer.Data;
using Dell.Finance.Framework.Core.Mailer.Data.Enum;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using OracleLoggerProvider;
using Steeltoe.Extensions.Configuration.CloudFoundry;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


namespace Dell.Finance.BR.InboundInvoice.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }
        AppSettings AppSettings { get; set; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            AddAppSettings(services);
            AddUnitOfWork(services);
            AddLogging(services);
            services.AddServices();
            services.AddValidator();
            AddAuthentication(services);
            AddMvc(services);
            AddSwaggerGen(services);
            AddCors(services);
            services.AddMailer();
            AddSmtpEmailconfiguration(services);
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseAuthentication();
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "InboundInvoiceApi");
            });
        }

        private void AddLogging(IServiceCollection services)
        {
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddDebug();
                if (AppSettings.UseSeq())
                {
                    loggingBuilder.AddSeq(AppSettings.LoggingServerUrl);
                }
                if (AppSettings.UseOracleProvider())
                {
                    UseOracleLoggerProvider(loggingBuilder);
                }
            });
        }

        private void UseOracleLoggerProvider(ILoggingBuilder loggingBuilder)
        {
            var logConfiguration = new LogConfiguration("SERVICE_FINANCE_LOG.DELL_FINANCE_LOG");
            logConfiguration.Add("SERVER_NAME", AppSettings.LoggingServerName?.ToLower());
            logConfiguration.Add("APPLICATION", "inboundinvoiceapi");
            logConfiguration.Add("LOG_DATE", LogValue.Date);
            logConfiguration.Add("LOG_LEVEL", LogValue.LogLevel);
            logConfiguration.Add("LOGGER", LogValue.SourceContext);
            logConfiguration.Add("MESSAGE", LogValue.State);
            logConfiguration.Add("EXCEPTION", LogValue.Exception);
            var oracleLoggerProvider = new OracleLogProvider(AppSettings.ConnectionString, logConfiguration);

            loggingBuilder.AddProvider(oracleLoggerProvider);
        }

        private void AddAppSettings(IServiceCollection services)
        {
            if (Environment.IsDevelopment())
            {
                this.AppSettings = Configuration.GetSection(nameof(AppSettings)).Get<AppSettings>();
            }
            else
            {
                this.AppSettings = SolvePCFAppSettings(services);
            }

            services.AddSingleton(this.AppSettings);
        }

        private AppSettings SolvePCFAppSettings(IServiceCollection services)
        {

            // Setup Options framework with DI
            services.AddOptions();
            // Add Steeltoe Cloud Foundry Options to service container
            services.ConfigureCloudFoundryOptions(Configuration);

            var serviceProvider = services.BuildServiceProvider();
            var serviceOptions = serviceProvider.GetService<IOptions<CloudFoundryServicesOptions>>();
            var serviceAppOptions = serviceProvider.GetService<IOptions<CloudFoundryApplicationOptions>>();
            var appSettingsService = serviceOptions.Value.ServicesList.Where(s => s.Name.StartsWith("AppSettings")).Single();

            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.AuthKey), out Credential authKey);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.ConnectionString), out Credential connectionString);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.LoggingProvider), out Credential loggingProvider);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.LoggingServerUrl), out Credential loggingServerUrl);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.FromEmail), out Credential fromEmail);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.ToEmail), out Credential toEmail);

            return new AppSettings()
            {
                AuthKey = authKey.Value,
                ConnectionString = connectionString.Value,
                LoggingProvider = loggingProvider.Value,
                LoggingServerUrl = loggingServerUrl.Value,
                LoggingServerName = serviceAppOptions.Value.ApplicationName,
                FromEmail = fromEmail.Value,
                ToEmail = toEmail.Value
            };
        }

        private void AddSmtpEmailconfiguration(IServiceCollection services)
        {
            SmtpConfiguration smtpConfig = null;
            if (Environment.IsDevelopment())
            {
                smtpConfig = Configuration.GetSection("SmtpConfiguration").Get<SmtpConfiguration>();
            }
            else
            {
                smtpConfig = SolvePCFSmtpConfiguration(services);
            }
            services.AddSingleton(smtpConfig);
        }

        private SmtpConfiguration SolvePCFSmtpConfiguration(IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var serviceOptions = serviceProvider.GetService<IOptions<CloudFoundryServicesOptions>>();
            var smtpConfigurationService = serviceOptions.Value.ServicesList.Where(s => s.Name.StartsWith("SmtpConfiguration")).Single();

            smtpConfigurationService.Credentials.TryGetValue(nameof(SmtpConfiguration.SmtpServer), out Credential smtpServer);
            smtpConfigurationService.Credentials.TryGetValue(nameof(SmtpConfiguration.SmtpPort), out Credential smtpPort);

            return new SmtpConfiguration()
            {
                SmtpServer = smtpServer.Value,
                SmtpPort = Enum.Parse<SmtpStandardPort>(smtpPort.Value)
            };
        }

        private void AddUnitOfWork(IServiceCollection services)
        {
            services.AddSynchroUnitOfWork(AppSettings.ConnectionString);
        }

        private void AddSwaggerGen(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "InboundInvoiceApi", Version = "v1" });

                var filePath = Path.Combine(System.AppContext.BaseDirectory, "docs.xml");
                c.IncludeXmlComments(filePath);

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Enter into 'Value': Bearer {token}",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    { "Bearer", new string[0] }
                });
            });
        }

        private void AddMvc(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            });
        }

        private void AddCors(IServiceCollection services)
        {
            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
        }

        private void AddAuthentication(IServiceCollection services)
        {
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = GetSecurityKey(),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        private SecurityKey GetSecurityKey()
        {
            var key = Encoding.UTF8.GetBytes(AppSettings.AuthKey);
            return new SymmetricSecurityKey(key);
        }

    }
}
