﻿using Microsoft.Extensions.DependencyInjection;
using Dell.Finance.BR.InboundInvoice.Services;
using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Newtonsoft.Json;
using Xunit;
using Dell.Finance.BR.InboundInvoice.Infrastructure.Data;
using Dell.Finance.Framework.Core.Mailer.Data;
using Dell.Finance.Framework.Core.Mailer.Service;
using Dell.Finance.Framework.Core.Mailer.Contracts;
using MailKit;
using MailKit.Net.Smtp;

namespace Dell.Finance.BR.InboundInvoice.Tests.Integration
{
    public class IntegrationTestBase
    {
        protected ServiceProvider ServiceProvider { get; set; }
        public IntegrationTestBase()
        {
            ConfigureServiceCollection();   
        }

        private void ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();
            AddAppSettings(serviceCollection);
            AddSmtpEmailconfiguration(serviceCollection);
            AddUnitOfWork(serviceCollection);
            serviceCollection.AddServices();
            serviceCollection.AddValidator();
            serviceCollection.AddLogging();
            AddMailer(serviceCollection);
            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        protected void AddAppSettings(IServiceCollection services)
        {
            AppSettings appSettings = new AppSettings();
            services.AddSingleton(appSettings);
        }

        protected void AddSmtpEmailconfiguration(IServiceCollection services)
        {
            SmtpConfiguration smtpConfig = new SmtpConfiguration();
            services.AddSingleton(smtpConfig);
        }

        protected void AddMailer(ServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IEmailService, SmtpEmailService>();
            serviceCollection.AddTransient<IMailTransport, SmtpClient>();
        }

        private void AddUnitOfWork(ServiceCollection services)
        {
            var appSettings = new AppSettingsTest();
            if (appSettings.UseMock)
            {
                services.AddFakeUnitOfWork();
            }
            else
            {
                services.AddSynchroUnitOfWork(appSettings.ConnectionString);
            }
        }

        protected void Equivalent(object expected, object atual)
        {
            var expectedStr = JsonConvert.SerializeObject(expected);
            var actualStr = JsonConvert.SerializeObject(atual);

            Assert.Equal(expectedStr, actualStr);
        }
    }
}
