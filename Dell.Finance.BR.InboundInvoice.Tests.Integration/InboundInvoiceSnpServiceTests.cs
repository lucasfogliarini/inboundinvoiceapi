﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Internal;
using NSubstitute;
using System;
using System.Collections.Generic;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Integration
{
    public class InboundInvoiceSnpServiceTests : IntegrationTestBase
    {
        private readonly IInboundInvoiceSNPService _inboundInvoiceSnpService;

        public InboundInvoiceSnpServiceTests()
        {
            _inboundInvoiceSnpService = ServiceProvider.GetService<IInboundInvoiceSNPService>();
        }

        [Fact]
        public void CreateInboundImportedSoftware_Should_Create_Inbound()
        {
            var outboundId = 1353834767001;
            SeedInvoice(outboundId);
            SeedSynParameter();
            SeedSynListValues();
            SeedInvoiceDollarRate(4);

            long inboundId = _inboundInvoiceSnpService.CreateInboundImportedSoftware(outboundId);

            Assert.True(inboundId > 0);
        }

        private void SeedInvoice(long invoiceid)
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Invoice()
            {
                DofSequence = invoiceid,
                Id = invoiceid,
                IndEntradaSaida = "S",
                EmitentePfjCodigo = "Emitente123",
                RemetentePfjCodigo = "Remetente123",
                DestinatarioPfjCodigo = "Destinatario123",
                DlbRecofNumOrder = "NumeroOrdem123",
                DellUsOrder = "123456789",//9chars
                SiglaMoeda = DollarRateService.SIGLA_MOEDA_USD,
                SisCodigo = DollarRateService.SIS_CODIGO_EDI,
                TxConversaoMoedaNac = 4,
                Items = new List<InvoiceItem>()
                {
                    new InvoiceItem()
                    {
                       IdfNum = 1,
                       MercCodigo = "MercCodigo1",
                       Qtd = 3,
                       InvoiceItemData = new InvoiceItemData()
                       {
                           FulfillmentLocId = "00",
                           PrecoUnitarioUSD = 13.5m
                       }
                    }
                }
            });
        }
        private void SeedSynParameter()
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new SynParameter()
            {
                ParCodigo = InboundInvoiceSNPService.MICROSOFT_SNP_MARKUP,
                Valor = "20"
            });
            unitOfWork.Add(new SynParameter()
            {
                ParCodigo = InboundInvoiceSNPService.VMWARE_SNP_MARKUP,
                Valor = "10"
            });
        }

        private void SeedSynListValues()
        {
            var listValues = new List<SynListValues>()
            {
                new SynListValues()
                {
                    ListaTipo = "VMWARE_SNP_PFJ_CODIGOS",
                    ListaValor = "123"
                },
                new SynListValues()
                {
                    ListaTipo = "VMWARE_SNP_PFJ_CODIGOS",
                    ListaValor = "456"
                },
                new SynListValues()
                {
                    ListaTipo = "MICROSOFT_SNP_PFJ_CODIGOS",
                    ListaValor = "321"
                },
                new SynListValues()
                {
                    ListaTipo = "MICROSOFT_SNP_PFJ_CODIGOS",
                    ListaValor = "544346F"
                }
            };
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.AddRange(listValues);
        }
        private void SeedInvoiceDollarRate(decimal dollarRate)
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Invoice()
            {
                DofSequence = 99999,
                TxConversaoMoedaNac = dollarRate,
                SiglaMoeda = DollarRateService.SIGLA_MOEDA_USD,
                SisCodigo = DollarRateService.SIS_CODIGO_EDI,
                DtFatoGeradorImposto = DateTime.Now
            });
        }
    }
}
