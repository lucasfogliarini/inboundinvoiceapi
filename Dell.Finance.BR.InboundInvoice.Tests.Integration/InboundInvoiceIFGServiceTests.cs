﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Internal;
using NSubstitute;
using System;
using System.Collections.Generic;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Integration
{
    public class InboundInvoiceIFGServiceTests : IntegrationTestBase
    {
        private readonly IInboundInvoiceIFGService _inboundInvoiceService;

        public InboundInvoiceIFGServiceTests()
        {
            _inboundInvoiceService = ServiceProvider.GetService<IInboundInvoiceIFGService>();
        }

        [Fact]
        public void CreateInboundImportedSoftwareShouldCreateInbound()
        {
            // Arrange
            var outboundId = 1264260672001;
            SeedSynListValues();
            SeedInvoiceItemData();
            SeedInvoice(outboundId);

            // Act
            long inboundId = _inboundInvoiceService.CreateInboundImportedSoftware(outboundId);

            // Assert
            Assert.True(inboundId > 0);
        }

        private void SeedInvoice(long invoiceid)
        {
            var systemClock = Substitute.For<ISystemClock>();
            systemClock.UtcNow.Returns(DateTime.Now.AddDays(-10));

            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Invoice()
            {
                CodigoDoSite = 1,
                Id = invoiceid,
                DofSequence = 00002,
                IndEntradaSaida = "S",
                EmitentePfjCodigo = "Emitente123",
                RemetentePfjCodigo = "67117F",
                DestinatarioPfjCodigo = "Destinatario123",
                DlbRecofNumOrder = "NumeroOrdem123",
                DellUsOrder = "123456789",//9chars
                DtFatoGeradorImposto = systemClock.UtcNow.Date,
                SiglaMoeda = DollarRateService.SIGLA_MOEDA_USD,
                SisCodigo = DollarRateService.SIS_CODIGO_EDI,
                TxConversaoMoedaNac = 4,
                Items = new List<InvoiceItem>()
                {
                    new InvoiceItem()
                    {
                       MercCodigo = "MercCodigo1",
                       Qtd = 3,
                       IdfNum = 00001,
                       InvoiceItemData = new InvoiceItemData()
                       {
                           FulfillmentLocId = "00",
                           PrecoUnitarioUSD = 13.5m
                       }
                    }
                }
            });
        }

        private void SeedInvoiceItemData()
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new InvoiceItemData
            {
                CodigoDoSite = 1,
                DofSequence = 00002,
                IdfNum = 00001,
                IpOwnerAttribute = "DELL"
            });
        }

        private void SeedSynListValues()
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new SynListValues
            {
                Description = "67117F",
                ListaTipo = "IP_OWNER_LIST",
                ListaValor = "DELL"
            });
        }
    }
}
