﻿namespace Dell.Finance.BR.InboundInvoice.Tests.Integration
{
    public class AppSettingsTest
    {
        public string ConnectionString { get; } = "User Id=SYNCHRO; Password=SYNCHRO_DEV; PERSIST SECURITY INFO=True;  Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=mge19de1dbscn.us.dell.com)(PORT=1521))(CONNECT_DATA=(SERVER=dedicated)(SERVICE_NAME=syncm.dev.amer.dell.com)))";
        public bool UseMock { get; } = true;
    }
}
