﻿using Dell.Finance.BR.InboundInvoice.Infrastructure;
using Dell.Finance.BR.InboundInvoice.Services;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dell.Finance.BR.InboundInvoice.Tests.Integration
{
    public class InboundInvoiceEMCServiceTests : IntegrationTestBase
    {
        private readonly IInboundInvoiceEMCService _inboundInvoiceService;

        public InboundInvoiceEMCServiceTests()
        {
            _inboundInvoiceService = ServiceProvider.GetService<IInboundInvoiceEMCService>();
        }

        [Fact]
        public void CreateInboundImportedSoftware_Should_Create_Inbound()
        {
            var outboundId = 1315318246001;
            SeedInvoice(outboundId);
            SeedSynParameter();

            long inboundId = _inboundInvoiceService.CreateInboundImportedSoftware(outboundId);

            Assert.True(inboundId > 0);
        }

        private void SeedInvoice(long invoiceid)
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Invoice()
            {
                Id = invoiceid,
                IndEntradaSaida = "S",
                EmitentePfjCodigo = "Emitente123",
                RemetentePfjCodigo = "Remetente123",
                DestinatarioPfjCodigo = "Destinatario123",
                DlbRecofNumOrder = "NumeroOrdem123",
                DellUsOrder = "123456789",//9chars
                Items = new List<InvoiceItem>()
                {
                    new InvoiceItem()
                    {
                       IdfNum = 1,
                       MercCodigo = "MercCodigo1",
                       Qtd = 3,
                       PrecoUnitario = 3.5m
                    }
                }
            });
        }

        private void SeedSynParameter()
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new SynParameter()
            {
                ParCodigo = InboundInvoiceEMCService.INBOUND_COST_RATE_SOFT_EMC,
                Valor = "68.06"
            });
        }
    }
}
